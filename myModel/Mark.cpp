//
// Created by dragoon on 30.11.2020.
//

#include "Mark.h"

Mark Mark::instances[7];
Mark Mark::field[19];

Mark::Mark() {
    this->fill(std::numeric_limits<int>::max());
    this->currentMark = 0;
}

bool Mark::putMark(const Vec2Int &pos) {
    if (Data::insideMap(pos)) {
        if (checkMarkUnsafe(pos)) {
            return false;
        }
        mark[pos.x][pos.y] = currentMark;
        return true;
    }
    return false;
}

bool Mark::putMarkUnsafe(const Vec2Int &pos) {
    if (checkMarkUnsafe(pos)) {
        return false;
    }
    mark[pos.x][pos.y] = currentMark;
    return true;
}

bool Mark::checkMarkUnsafe(const Vec2Int &pos) const {
    return mark[pos.x][pos.y] == currentMark;
}

bool Mark::checkMark(const Vec2Int &pos) const {
    if (!Data::insideMap(pos)) {
        return false;
    }
    return checkMarkUnsafe(pos);
}

void Mark::fill(int value) {
    for (int i = 0; i != Data::mapSize; ++i) {
        for (int j = 0; j != Data::mapSize; ++j) {
            mark[i][j] = value;
        }
    }
}

bool Mark::newMinimumUnsafe(const Vec2Int &pos, int value) {
    if (mark[pos.x][pos.y] > value) {
        mark[pos.x][pos.y] = value;
        return true;
    }
    return false;
}

void Mark::putValUnsafe(const Vec2Int &pos, int value) {
    mark[pos.x][pos.y] = value;
}

void Mark::justPutMark(const Vec2Int &pos) {
    if (Data::insideMap(pos)) {
        mark[pos.x][pos.y] = currentMark;
    }
}

void Mark::justPutMarkUnsafe(const Vec2Int &pos) {
    mark[pos.x][pos.y] = currentMark;
}

int Mark::getMarkUnsafe(const Vec2Int &pos) const {
    return mark[pos.x][pos.y];
}

void Mark::addUnsafe(const Vec2Int &pos, int val) {
    mark[pos.x][pos.y] += val;
}

void Mark::incrementUnsafe(const Vec2Int &pos) {
    ++mark[pos.x][pos.y];
}

void Mark::decrementUnsafe(const Vec2Int &pos) {
    --mark[pos.x][pos.y];
}

bool Mark::nonZeroUnsafe(const Vec2Int &pos) {
    return mark[pos.x][pos.y] > 0;
}

void Mark::copy(Mark &other) {
    for (int i = 0; i != Data::mapSize; ++i) {
        for (int j = 0; j != Data::mapSize; ++j) {
            this->mark[i][j] = other.mark[i][j];
        }
    }
}
