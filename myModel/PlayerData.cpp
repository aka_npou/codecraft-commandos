//
// Created by dragoon on 28.11.2020.
//

#include "PlayerData.h"
#include "Utils.h"

const int &PlayerData::id() const {
    return player.id;
}

inline void updateVector(std::vector<MyEntity> &prevStep,
                         const PlayerView &playerView,
                         int playerId,
                         EntityType entityType) {
    std::unordered_set<int> existIds;
    std::vector<MyEntity> updated;
    for (const auto &entity : playerView.entities) {
        if (entity.entityType != entityType || playerId != *entity.playerId) {
            continue;
        }
        updated.emplace_back(entity, playerView.currentTick);
        existIds.insert(entity.id);
    }
    if (playerView.myId == playerId) {
        int currUpdated = 0;
        for (const auto &prev : prevStep) {
            if (existIds.find(prev.id) == existIds.end()) {
                ++Data::deaths[prev.entityType];
                Data::damageReceived[prev.entityType] += prev.health;
                continue;
            }
            while (updated[currUpdated].id != prev.id) {
                ++currUpdated;
            }
            const auto &upd = updated[currUpdated];
            if (prev.canMove()) { // посчитаем лечения юнитов
                Data::healCounter += prev.actionsCount;
            }
            Data::damageReceived[prev.entityType] += prev.health - upd.health - prev.actionsCount;
        }

        prevStep.swap(updated);
        return;
    }
    if (!playerView.fogOfWar) {
        prevStep.swap(updated);
        return;
    }

    auto &visibleField = Mark::field[VISIBLE];
    for (const auto &old: prevStep) {
        if (existIds.find(old.id) != existIds.end() || Utils::unitVisible(old)) {
            continue;
        }
        updated.emplace_back(old);
    }
    prevStep.swap(updated);
}

void PlayerData::update(const PlayerView &playerView) {
    this->totalHouses = 0;
    this->totalUnits = 0;
    for (const auto &playerItem : playerView.players) {
        if (playerItem.id == this->player.id) {
            this->dRes = playerItem.resource - this->player.resource;
            this->dScore = playerItem.score - this->player.score;
            if (this->dRes < -350) {
                this->startedBarrack = true;
            }
            this->player = playerItem;
            break;
        }
    }

    auto update = [&playerView, playerId = player.id](std::vector<MyEntity> &entity, EntityType entityType) {
        updateVector(entity, playerView, playerId, entityType);
    };
    newBuildings.clear();

    update(workers, BUILDER_UNIT);
    update(builderBase, BUILDER_BASE);

    update(rangedUnits, RANGED_UNIT);
    update(rangedBase, RANGED_BASE);

    update(meleeUnits, MELEE_UNIT);
    update(meleeBase, MELEE_BASE);

    update(walls, WALL);
    update(houses, HOUSE);
    update(turrets, TURRET);

    for (const auto &entity : builderBase) {
        totalHouses += entity.active * MyEntity::props[BUILDER_BASE].populationProvide;
    }
    for (const auto &entity : meleeBase) {
        totalHouses += entity.active * MyEntity::props[MELEE_BASE].populationProvide;
    }
    for (const auto &entity : rangedBase) {
        totalHouses += entity.active * MyEntity::props[RANGED_BASE].populationProvide;
    }
    for (const auto &entity : houses) {
        totalHouses += entity.active * MyEntity::props[HOUSE].populationProvide;
    }
    totalUnits += (int) workers.size() * MyEntity::props[BUILDER_UNIT].populationUse;
    totalUnits += (int) rangedUnits.size() * MyEntity::props[RANGED_UNIT].populationUse;
    totalUnits += (int) meleeUnits.size() * MyEntity::props[MELEE_UNIT].populationUse;
}

void PlayerData::applyForAll(const std::function<void(std::vector<MyEntity> &)> &func) {
    func(workers);
    func(rangedUnits);
    func(walls);
    func(houses);
    func(builderBase);
    func(meleeBase);
    func(rangedBase);
    func(turrets);
    func(meleeUnits);
}

PlayerData::PlayerData() : startedBarrack(false) {
}
