//
// Created by dragoon on 30.11.2020.
//

#ifndef AICUP2020_MARK_H
#define AICUP2020_MARK_H


#include "Data.h"

enum FieldType {
    FOOD_DISTANCE = 0, // std::numeric_limits<int>::max()
    ENEMY_INFLUENCE_FIELD = 1, // std::numeric_limits<int>::max()
    MY_WORKERS_DISTANCE_FIELD = 2, // std::numeric_limits<int>::max()
    ENEMY_WORKERS_DISTANCE_FIELD = 3, // std::numeric_limits<int>::max()
    MY_DISTANCE_FIELD = 4,
    ENEMY_MIL_DISTANCE_FIELD = 5,
    ENEMY_CIVILIAN_DISTANCE_FIELD = 6,
    ENEMY_DANGER_FIELD = 7,
    ENEMY_DANGER_EXTENDED_FIELD = 8,
    ENEMY_DANGER_EXTENDED_FIELD_2 = 9,
    PATH_HOTSPOT = 10,
    LAST_PATH_HOTSPOT = 11,
    BLOCK_STATIC = 12,
    RESOURCE_DIG_MARK = 13,
    INFLUENCE_FIELD = 14,
    VISIBLE = 15,
    LAST_SAW = 16,
    CIV_TIME_TO_GO = 17,
    MIL_TIME_TO_GO = 18,
    REPAIR_PLACES = 19
//    ME_DANGER_FIELD = 14,
//    ME_DANGER_EXTENDED_FIELD = 15,
//    ME_DANGER_EXTENDED_FIELD_2 = 16

};

class Mark {
public:
    static const int NO_ENEMY_DISTANCE = 10000;

    int mark[80][80];
    int currentMark;

    static Mark instances[7];
    static Mark field[19];

    Mark();

    bool putMark(const Vec2Int &pos);

    void fill(int value);

    void copy(Mark &other);

    bool checkMark(const Vec2Int &pos) const;

    bool checkMarkUnsafe(const Vec2Int &pos) const;

    int getMarkUnsafe(const Vec2Int &pos) const;

    void putValUnsafe(const Vec2Int &pos, int value);

    void justPutMark(const Vec2Int &pos);

    void justPutMarkUnsafe(const Vec2Int &pos);

    bool newMinimumUnsafe(const Vec2Int &pos, int value);

    bool putMarkUnsafe(const Vec2Int &anInt);

    void addUnsafe(const Vec2Int &pos, int val);

    void incrementUnsafe(const Vec2Int &anInt);

    void decrementUnsafe(const Vec2Int &anInt);

    bool nonZeroUnsafe(const Vec2Int &pos);
};


#endif //AICUP2020_MARK_H
