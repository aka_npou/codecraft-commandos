//
// Created by dragoon on 07.12.2020.
//

#ifndef AICUP2020_PATHMAP_H
#define AICUP2020_PATHMAP_H

#include "Data.h"

namespace PathMap {

    const int pathfindCollisionDeep = 400;

    static int mark[80][80][pathfindCollisionDeep];
    static int pathfind[80][80][pathfindCollisionDeep];
    static int pathfindMark = 0;
    static int currentMark = 0;

    static void init() {
        pathfindMark = std::numeric_limits<int>::min();
        for (int i = 0; i != 80; ++i) {
            for (int j = 0; j != 80; ++j) {
                for (int k = 0; k != pathfindCollisionDeep; ++k) {
                    mark[i][j][k] = -1;
                    pathfind[i][j][k] = std::numeric_limits<int>::min();
                }
            }
        }
    }

    static void nextTick() {
        currentMark += 5;
    }

    static Vec2Int directions[5] = {{1,  0},
                                    {0,  1},
                                    {-1, 0},
                                    {0,  -1},
                                    {0,  0}};

    static Vec2Int direction(int idx) {
        return directions[idx];
    }

    static Vec2Int reverseDirection(int idx) {
#ifdef DEBUG_ENABLED
        if (idx < 0) {
            std::cerr << "ататааааа!!!!" << std::endl;
            throw std::exception();
        }
#endif
        return directions[(idx + 2) & 3];
    }

    static int pathfindDirection(const Vec2Int &position, int time) {
        return pathfind[position.x][position.y][time] - pathfindMark;
    }

    static void putPathfindDirection(const Vec2Int &pos, int time, int dir) {
        if (time >= pathfindCollisionDeep) {
            return;
        }
#ifdef DEBUG_ENABLED
        if (pathfind[pos.x][pos.y][time] >= pathfindMark) {
            std::cerr << "Перезапись направления, ёлки палки!!!!" << std::endl;
            throw std::exception();
        }
#endif
        pathfind[pos.x][pos.y][time] = pathfindMark + dir;
    }

    static int directionFrom(const Vec2Int &position, int time) {
        return mark[position.x][position.y][time] - currentMark;
    }

    inline static void eraseDirection(const Vec2Int &pos, int time) {
//        MapDrawing::drawSquare(pos);
#ifdef DEBUG_ENABLED
        if (mark[pos.x][pos.y][time] < currentMark) {
            std::cerr << "Очищаем чистое???" << std::endl;
            throw std::exception();
        }
#endif
        mark[pos.x][pos.y][time] = currentMark - 1;
    }

    inline static void putDirection(const Vec2Int &pos, int time, int dir) {
        if (time >= pathfindCollisionDeep) {
            return;
        }
#ifdef DEBUG_ENABLED
        if (mark[pos.x][pos.y][time] >= currentMark) {
            std::cerr << "Перезапись направления, ёлки палки!!!!" << std::endl;
            throw std::exception();
        }
#endif
        mark[pos.x][pos.y][time] = currentMark + dir;
    }

    inline static bool couldGoHere(const Vec2Int &to, int time, const Vec2Int &from) {
        int dir = PathMap::directionFrom(to, time);
        if (dir >= 0) { // ага, тут хтота есть
            return false;
        }
        // а что насчёт встречного пути в мою клетку?
        int check = 8;
        Vec2Int cycleCheckPos = from;
        dir = PathMap::directionFrom(cycleCheckPos, time);
        while (check >= 0 && dir >= 0) {
#ifdef DEBUG_ENABLED
            if (dir > 4) {
                std::cerr << "Ну не мог тут кто-то стоять...." << std::endl;
                throw std::exception();
            }
#endif
            Vec2Int newPos = cycleCheckPos + PathMap::reverseDirection(dir);
            // тут добавить проверку на то, что мы стоим. на всякий случай
            // хм. Да не, вроде не надо

            // Ммм. Если цикл дошёл до клетки, в которую я собираюсь - аяй
            if (newPos == to) {
                return false;
            }
            cycleCheckPos = newPos;
            dir = PathMap::directionFrom(cycleCheckPos, time);
            --check;
        }
        return true;
    }
}


#endif //AICUP2020_PATHMAP_H
