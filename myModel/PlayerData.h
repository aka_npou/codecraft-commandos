//
// Created by dragoon on 28.11.2020.
//

#ifndef AICUP2020_PLAYERDATA_H
#define AICUP2020_PLAYERDATA_H


#include <vector>
#include <functional>
#include "../model/Entity.hpp"
#include "../model/Player.hpp"
#include "../model/PlayerView.hpp"
#include "MyEntity.h"

class PlayerData {

public:

    PlayerData();

    void update(const PlayerView &playerView);

    Player player;
    int totalHouses;
    int totalUnits;
    int dRes;
    int dScore;
    bool startedBarrack;

    std::vector<MyEntity> workers;
    std::vector<MyEntity> rangedUnits;
    std::vector<MyEntity> walls;
    std::vector<MyEntity> houses;
    std::vector<MyEntity> builderBase;
    std::vector<MyEntity> meleeBase;
    std::vector<MyEntity> rangedBase;
    std::vector<MyEntity> turrets;
    std::vector<MyEntity> meleeUnits;
    std::vector<MyEntity> newBuildings;

    const int &id() const;

    void applyForAll(const std::function<void(std::vector<MyEntity> &)> &func);

};


#endif //AICUP2020_PLAYERDATA_H
