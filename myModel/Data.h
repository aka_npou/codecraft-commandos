//
// Created by dragoon on 28.11.2020.
//

#ifndef AICUP2020_DATA_H
#define AICUP2020_DATA_H


#include <limits>
#include <iostream>
#include "MyEntity.h"
#include "PlayerData.h"

struct Data {
    static MyEntity *map[80][80];
    static int myId;
    static int healCounter;
    static int maxEntityId;
    static int maxRetries;
    static int expectedResource;

    static int kills[TURRET + 1];
    static int attack[TURRET + 1];
    static int overDamage;
    static int damageReceived[TURRET + 1];
    static int deaths[TURRET + 1];
    static PlayerData me;
    static std::vector<PlayerData> others;
    static std::vector<MyEntity> resources;

    static MyEntity *fakeObstacle() {
        static auto myEntity = new MyEntity(Entity(-1, nullptr, TURRET, Vec2Int(-1, -1), std::numeric_limits<int>::max(), false));
        return myEntity;
    }

    static MyEntity *border() {
        static auto myEntity = new MyEntity(Entity(-1, nullptr, UNKNOWN, Vec2Int(-1, -1), std::numeric_limits<int>::max(), false));
        return myEntity;
    }

    static int mapSize;

    static void init() {
        for (int i = 0; i <= TURRET; ++i) {
            kills[i] = 0;
            attack[i] = 0;
            damageReceived[i] = 0;
            deaths[i] = 0;
        }
        overDamage = 0;
    }

    static void initMap(int size) {
        Data::mapSize = size;
        for (int i = 0; i != size; ++i) {
            for (int j = 0; j != size; ++j) {
                map[i][j] = nullptr;
            }
        }
    }

    inline static bool insideMap(const Vec2Int &pos) {
        return insideMap(pos.x, pos.y);
    }

    inline static bool insideMap(int x, int y) {
        return x >= 0 && x < mapSize && y >= 0 && y < mapSize;
    }

    inline static MyEntity *getUnsafe(int x, int y) {
        return map[x][y];
    }

    inline static MyEntity *getUnsafe(const Vec2Int &pos) {
        return map[pos.x][pos.y];
    }

    inline static MyEntity *get(int x, int y) {
        if (insideMap(x, y)) {
            return map[x][y];
        } else {
            return border();
        }
    }

    inline static MyEntity *get(const Vec2Int &pos) {
        return get(pos.x, pos.y);
    }

    inline static void addToMap(MyEntity &src) {
        int size = src.size();
        auto const &pos = src.position;
        if (size == 1) {
            Data::map[pos.x][pos.y] = &src;
        } else {
            for (int i = 0; i != size; ++i) {
                for (int j = 0; j != size; ++j) {
                    Data::map[pos.x + i][pos.y + j] = &src;
                }
            }
        }
    }

    inline static void addAllToMap(std::vector<MyEntity> &src) {
        for (int x = 0; x != (int) src.size(); ++x) {
            addToMap(src[x]);
        }
    }
};


#endif //AICUP2020_DATA_H
