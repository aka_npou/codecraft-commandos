//
// Created by dragoon on 28.11.2020.
//

#ifndef AICUP2020_UTILS_H
#define AICUP2020_UTILS_H

#ifdef REWIND_VIEWER

#include "../rewind/MapDrawing.h"

#endif

#include <queue>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>
#include "Data.h"
#include "MyEntity.h"
#include "Mark.h"
#include "PathMap.h"
#include <random>
#include <functional>

namespace Utils {
    static std::random_device rd;
    static std::mt19937 g(rd());

#if(defined DEBUG_ENABLED && defined DEBUG_PATH)
    extern int pathToDraw;
#endif

    const int64_t pathScore[200] = {0, 999999, 1999997, 2999994, 3999990, 4999985, 5999979, 6999972, 7999964, 8999955,
                                    9999945, 10999934, 11999922, 12999909, 13999895, 14999880, 15999864, 16999847,
                                    17999829, 18999810, 19999790, 20999769, 21999747, 22999724, 23999700, 24999675,
                                    25999649, 26999622, 27999594, 28999565, 29999535, 30999504, 31999472, 32999439,
                                    33999405, 34999370, 35999334, 36999297, 37999259, 38999220, 39999180, 40999139,
                                    41999097, 42999054, 43999010, 44998965, 45998919, 46998872, 47998824, 48998775,
                                    49998725, 50998674, 51998622, 52998569, 53998515, 54998460, 55998404, 56998347,
                                    57998289, 58998230, 59998170, 60998109, 61998047, 62997984, 63997920, 64997855,
                                    65997789, 66997722, 67997654, 68997585, 69997515, 70997444, 71997372, 72997299,
                                    73997225, 74997150, 75997074, 76996997, 77996919, 78996840, 79996760, 80996679,
                                    81996597, 82996514, 83996430, 84996345, 85996259, 86996172, 87996084, 88995995,
                                    89995905, 90995814, 91995722, 92995629, 93995535, 94995440, 95995344, 96995247,
                                    97995149, 98995050, 99994950, 100994849, 101994747, 102994644, 103994540, 104994435,
                                    105994329, 106994222, 107994114, 108994005, 109993895, 110993784, 111993672,
                                    112993559, 113993445, 114993330, 115993214, 116993097, 117992979, 118992860,
                                    119992740, 120992619, 121992497, 122992374, 123992250, 124992125, 125991999,
                                    126991872, 127991744, 128991615, 129991485, 130991354, 131991222, 132991089,
                                    133990955, 134990820, 135990684, 136990547, 137990409, 138990270, 139990130,
                                    140989989, 141989847, 142989704, 143989560, 144989415, 145989269, 146989122,
                                    147988974, 148988825, 149988675, 150988524, 151988372, 152988219, 153988065,
                                    154987910, 155987754, 156987597, 157987439, 158987280, 159987120, 160986959,
                                    161986797, 162986634, 163986470, 164986305, 165986139, 166985972, 167985804,
                                    168985635, 169985465, 170985294, 171985122, 172984949, 173984775, 174984600,
                                    175984424, 176984247, 177984069, 178983890, 179983710, 180983529, 181983347,
                                    182983164, 183982980, 184982795, 185982609, 186982422, 187982234, 188982045,
                                    189981855, 190981664, 191981472, 192981279, 193981085, 194980890, 195980694,
                                    196980497, 197980299, 198980100};

    inline static int64_t distanceScore(int pos) {
        return pos < 200 ? pathScore[pos] : pathScore[199];
    }

    inline static int mirrorCoord(int x) {
        return Data::mapSize - 1 - x;
    }

    inline static std::vector<Vec2Int> allMirroredPoints(const Vec2Int &pos, int playersCount) {
        if (playersCount == 2) {
            return {Vec2Int(mirrorCoord(pos.x), mirrorCoord(pos.y))};
        } else {
            return {Vec2Int(pos.y, mirrorCoord(pos.x)),
                    Vec2Int(mirrorCoord(pos.x), mirrorCoord(pos.y)),
                    Vec2Int(mirrorCoord(pos.y), pos.x)};
        }
    }

    static std::vector<Vec2Int> neighbours(const Vec2Int &pos, int size) {
        std::vector<Vec2Int> buildPositions;
        buildPositions.reserve(size * 4);
        for (int i = 0; i != size; ++i) {
            buildPositions.emplace_back(pos.x + i, pos.y - 1);
            buildPositions.emplace_back(pos.x - 1, pos.y + i);
            buildPositions.emplace_back(pos.x + i, pos.y + size);
            buildPositions.emplace_back(pos.x + size, pos.y + i);
        }
//        std::sort(buildPositions.begin(), buildPositions.end());
        return buildPositions;
    }

    static std::vector<Vec2Int> neighbours(const Vec2Int &pos, EntityType entityType) {
        return neighbours(pos, MyEntity::props[entityType].size);
    }

    inline void filterObstacles(std::vector<Vec2Int> &vector) {
        vector.erase(std::remove_if(vector.begin(), vector.end(), [](const auto &item) {
                         return Data::get(item) != nullptr;
                     }),
                     vector.end());
    }

    template <typename T>
    inline void eraseMatched(std::vector<T> &vector, const std::function<bool(const T &pos)> &func) {
        vector.erase(std::remove_if(vector.begin(), vector.end(), func), vector.end());
    }

    inline static void filterBorders(std::vector<Vec2Int> &vector) {
        vector.erase(std::remove_if(vector.begin(), vector.end(), [](const auto &item) {
                         return !Data::insideMap(item);
                     }),
                     vector.end());
    }

    inline static bool unitVisible(const MyEntity &entity) {
        auto &fogOfWar = Mark::field[VISIBLE];
        int size = entity.size();
        if (size == 1) {
            return fogOfWar.checkMarkUnsafe(entity.position);
        }
        for (int i = 0; i != size; ++i) {
            for (int j = 0; j != size; ++j) {
                if (fogOfWar.checkMarkUnsafe(entity.position + Vec2Int(i, j))) {
                    return true;
                }
            }
        }
        return false;
    }

    inline static bool isEnemy(const MyEntity *entity) {
        return entity != nullptr && entity->playerId && entity->playerId != Data::myId;
    }

    inline static void expandBorders(std::vector<Vec2Int> &vector, Mark &marker) {
        std::vector<Vec2Int> nextGen;
        nextGen.reserve(vector.size() + 4);
        for (const auto &pos : vector) {
            if (Data::get(pos) != nullptr) {
                continue;
            }
            for (const auto &nPos : pos.neighbours()) {
                if (marker.putMark(nPos)) {
                    nextGen.push_back(nPos);
                }
            }
        }
        nextGen.swap(vector);
    }

    inline static void expandBordersIgnoreMoveable(std::vector<Vec2Int> &vector, Mark &marker) {
        std::vector<Vec2Int> nextGen;
        nextGen.reserve(vector.size() + 4);
        for (const auto &pos : vector) {
            for (const auto &nPos : pos.neighbours()) {
                if (marker.putMark(nPos)) {
                    auto entity = Data::getUnsafe(nPos);
                    if (entity != nullptr && !entity->canMove()) {
                        continue;
                    }
                    nextGen.push_back(nPos);
                }
            }
        }
        nextGen.swap(vector);
    }

    inline static void expandIgnoreObstacles(std::vector<Vec2Int> &pos, Mark &marker) {
        std::vector<Vec2Int> newPos;
        newPos.reserve(pos.size() + 4);
        for (const auto &curr : pos) {
            for (const auto &ngb : curr.neighbours()) {
                if (marker.putMark(ngb)) {
                    newPos.push_back(ngb);
                }
            }
        }
        newPos.swap(pos);
    }

    inline static void fillMark(const Vec2Int &pos, int size, Mark &marker) {
        ++marker.currentMark;
        for (int i = 0; i != size; ++i) {
            for (int j = 0; j != size; ++j) {
                marker.mark[pos.x + i][pos.y + j] = marker.currentMark;
            }
        }
    }

    inline static void doForAllPts(const Vec2Int &pos, int size, const std::function<void(const Vec2Int &pos)> &func) {
        for (int i = 0; i != size; ++i) {
            for (int j = 0; j != size; ++j) {
                func({pos.x + i, pos.y + j});
            }
        }
    }

    inline static void fillMark(const Vec2Int &pos, EntityType entityType, Mark &marker) {
        fillMark(pos, MyEntity::props[entityType].size, marker);
    }

    inline static void spreadDistance(std::queue<Vec2Int> &queue, Mark &mark) {
        while (!queue.empty()) {
            auto curr = queue.front();
            int nVal = mark.mark[curr.x][curr.y] + 1;
            queue.pop();
            for (const auto &ngb : curr.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto entity = Data::getUnsafe(ngb);
                if ((entity == nullptr || entity->canMove()) && mark.newMinimumUnsafe(ngb, nVal)) {
                    queue.push(ngb);
                }
            }
        }
    }

    inline static void distanceFromBase(Mark &mark) {
        std::queue<Vec2Int> queue;
        int size = MyEntity::props[BUILDER_BASE].size;
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                mark.mark[i][j] = std::numeric_limits<int>::max();
            }
        }
        for (int i = 5; i != 5 + size; ++i) {
            for (int j = 5; j != 5 + size; ++j) {
                mark.mark[i][j] = 0;
                queue.emplace(i, j);
            }
        }
        spreadDistance(queue, mark);
    }

    inline static void spreadDistance(Mark &mark) {
        std::queue<Vec2Int> queue;
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.mark[i][j] == 0) {
                    queue.emplace(i, j);
                }
            }
        }
        spreadDistance(queue, mark);
    }

    inline static void applyForAllCoordsInRange(int range,
                                                const Vec2Int &pos,
                                                const std::function<void(const Vec2Int &coord)> &func) {
        int fromX = std::max(pos.x - range, 0);
        int toX = std::min(pos.x + range, Data::mapSize - 1);
        std::vector<MyEntity *> enemies;
        for (int x = fromX; x <= toX; ++x) {
            int absDx = std::abs(pos.x - x);
            int fromY = std::max(pos.y - range + absDx, 0);
            int toY = std::min(pos.y + range - absDx, Data::mapSize - 1);
            for (int y = fromY; y <= toY; ++y) {
                func({x, y});
            }
        }
    }

    inline static std::vector<MyEntity *> getAchievableEnemies(int attackRange, const Vec2Int &pos) {
        std::vector<MyEntity *> enemies;
        applyForAllCoordsInRange(attackRange, pos, [&enemies](const Vec2Int &pos) {
            auto entity = Data::getUnsafe(pos);
            if (Utils::isEnemy(entity)) {
                enemies.push_back(entity);
            }
        });
        return enemies;
    }

    inline static std::vector<MyEntity *> getAchievableEnemies(int attackRange, const MyEntity &unit) {
        if (unit.size() == 1) {
            return getAchievableEnemies(attackRange, unit.position);
        }
        auto &marker = Mark::instances[0];
        std::vector<MyEntity *> enemies;
        auto distancePositions = Utils::neighbours(unit.position, unit.entityType);
        fillMark(unit.position, unit.entityType, marker);
        for (const auto &position : distancePositions) {
            if (!Data::insideMap(position)) {
                continue;
            }
            marker.putMarkUnsafe(position);
            auto entity = Data::getUnsafe(position);
            if (Utils::isEnemy(entity)) {
                enemies.push_back(entity);
            }
        }
        for (int i = 1; i != attackRange; ++i) {
            Utils::expandIgnoreObstacles(distancePositions, marker);
            for (const auto &position : distancePositions) {
                auto entity = Data::getUnsafe(position);
                if (Utils::isEnemy(entity)) {
                    enemies.push_back(entity);
                }
            }
        }
        return enemies;
    }

    inline static void doForAllInUnitRange(int range,
                                           const Vec2Int &pos,
                                           const int size,
                                           const std::function<void(const Vec2Int &coord)> &func) {
        int fromX = std::max(pos.x - range, 0);
        int toX = std::min(pos.x + range + size - 1, Data::mapSize - 1);
        std::vector<MyEntity *> enemies;
        for (int x = fromX; x <= toX; ++x) {
            int absDx = x - pos.x;
            if (absDx > 0) {
                absDx = std::max(0, absDx - size + 1);
            } else {
                absDx = -absDx;
            }
            int fromY = std::max(pos.y - range + absDx, 0);
            int toY = std::min(pos.y + range - absDx + size - 1, Data::mapSize - 1);
            for (int y = fromY; y <= toY; ++y) {
                func({x, y});
            }
        }
    }

    inline static bool hasEnemiesInAttackRange(int attackRange, const MyEntity &unit) {
        auto &marker = Mark::instances[0];
        ++marker.currentMark;
        auto distancePositions = Utils::neighbours(unit.position, unit.entityType);
        fillMark(unit.position, unit.entityType, marker);
        for (const auto &position : distancePositions) {
            if (!Data::insideMap(position)) {
                continue;
            }
            marker.putMarkUnsafe(position);
            auto entity = Data::getUnsafe(position);
            if (Utils::isEnemy(entity)) {
                return true;
            }
        }
        for (int i = 1; i != attackRange; ++i) {
            Utils::expandIgnoreObstacles(distancePositions, marker);
            for (const auto &position : distancePositions) {
                auto entity = Data::getUnsafe(position);
                if (Utils::isEnemy(entity)) {
                    return true;
                }
            }
        }
        return false;
    }

    inline static int resourcesForBuilding(EntityType type, int currentCount, int needed) {
        int baseCost = MyEntity::props[type].cost + currentCount;
        return baseCost * needed + (needed * (needed - 1)) / 2;
    }

    inline static bool noneOrMoveableAlly(const MyEntity *entity, int playerId) {
        return entity == nullptr || (entity->playerId == playerId && entity->canMove());
    }

    // distance from point to smth with size
    inline static int distanceTo(const Vec2Int &from, const Vec2Int &to, int size) {
        int dX = from.x - to.x;
        if (dX > 0) {
            dX = std::max(0, dX - size + 1);
        }
        int dY = from.y - to.y;
        if (dY > 0) {
            dY = std::max(0, dY - size + 1);
        }
        return std::abs(dX) + std::abs(dY);
    }

    inline static std::array<int, 5> shuffledOrder() {
        std::array<int, 5> a = {0, 1, 2, 3, 4};
        std::shuffle(a.begin(), a.end(), g);
        return a;
    }

    inline static std::array<int, 5> semiShuffledOrder() {
        std::array<int, 5> a = {0, 1, 2, 3, 4};
        std::shuffle(a.begin(), a.begin() + 4, g);
        return a;
    }

    inline static void removePath(MyEntity &movedEntity) {
#ifdef DEBUG_ENABLED
        if (!movedEntity.moved()) {
            std::cerr << "Error while delete path" << std::endl;
            throw std::exception();
        }
#endif
        auto &pathHotspot = Mark::field[PATH_HOTSPOT];
        pathHotspot.decrementUnsafe(movedEntity.nextPt);
        PathMap::eraseDirection(movedEntity.nextPt, 0);
        int time = 1;
        Vec2Int pos = movedEntity.nextPt;
        movedEntity.nextPt = {-1, -1};
        bool found = true;
        while (found && time < PathMap::pathfindCollisionDeep) {
            found = false;
            for (int i = 0; i != 5; ++i) {
                auto newPos = PathMap::direction(i) + pos;
                if (PathMap::directionFrom(newPos, time) == i) {
                    pos = newPos;
                    found = true;
                    pathHotspot.decrementUnsafe(pos);
                    PathMap::eraseDirection(pos, time++);
                    break;
                }
            }
        }
    }

    inline static MyEntity *findUnit(Vec2Int unitPos, int time) { // Так... надо найти юнита(
        while (time != -1) {
            int dir = PathMap::directionFrom(unitPos, time);
#ifdef DEBUG_ENABLED
            if (dir < 0) {
                std::cerr << "Error while delete path" << std::endl;
                throw std::exception();
            }
#endif
            if (dir != 4) {
                unitPos = unitPos + PathMap::reverseDirection(dir);
            }
            --time;
        }
        auto unit = Data::getUnsafe(unitPos);
#ifdef DEBUG_ENABLED
        if (unit == nullptr) {
            std::cerr << "Error while delete path" << std::endl;
            throw std::exception();
        }
#endif
        return unit;
    }

    inline static std::pair<Vec2Int, int> getLastStepForTime(MyEntity &movedEntity, int maxTime) {
        if (movedEntity.nextPt.x < 0) {
            return std::make_pair(movedEntity.position, -1);
        }
        Vec2Int pos = movedEntity.position;
        int time = 0;
        bool found = true;
        while (found && time <= maxTime) {
            found = false;
            for (int i = 0; i != 5; ++i) {
                auto newPos = PathMap::direction(i) + pos;
                if (PathMap::directionFrom(newPos, time) == i) {
                    pos = newPos;
                    found = true;
                    ++time;
                    break;
                }
            }
        }
        return std::make_pair(pos, time - 1);
    }

    inline static void forMeAndNeighbours(const Vec2Int &pos,
                                          const std::function<void(const Vec2Int &checkPos)> &func) {
        func(pos);
        if (pos.x + 1 < Data::mapSize) {
            func({pos.x + 1, pos.y});
        }
        if (pos.y + 1 < Data::mapSize) {
            func({pos.x, pos.y + 1});
        }
        if (pos.x - 1 >= 0) {
            func({pos.x - 1, pos.y});
        }
        if (pos.y - 1 >= 0) {
            func({pos.x, pos.y - 1});
        }
    }

    struct PathPrior {
        int priority;
        int step;
        Vec2Int coord;

        PathPrior(int priority, int step, const Vec2Int &coord) : priority(
                (priority * 2 + (Data::getUnsafe(coord) != nullptr)) * 10000 - step), step(step), coord(coord) {}

        bool operator<(const PathPrior &other) const {
            return this->priority > other.priority; // lower is better
        }
    };

    // ЮЗАЕТ ТОЛЬКО СТАТИК БЛОКИ!
    inline static Vec2Int applyPathFind(MyEntity &entity,
                                        std::vector<Vec2Int> &destination,
                                        const std::function<int(const Vec2Int &)> &distance) {

        std::function<int(const Vec2Int &)> timeToGoThrough;
        auto &staticBlockField = Mark::field[BLOCK_STATIC];
        switch (entity.entityType) {
            case MELEE_UNIT:
            case RANGED_UNIT:
                timeToGoThrough = [](const Vec2Int &pos) -> int {
                    auto entity = Data::getUnsafe(pos);
                    if (entity == nullptr || entity->canMove()) {
                        return 1;
                    }
                    if (entity->entityType == RESOURCE) {
                        return 1 + (entity->health + 4) / 5;
                    }
                    return 0;
                };
                break;
            default:
                timeToGoThrough = [&staticBlockField](const Vec2Int &pos) -> int {
                    return staticBlockField.checkMarkUnsafe(pos) ? 0 : 1;
                };
                break;
        }
        std::priority_queue<PathPrior> queue;

        auto &pathHotspot = Mark::field[PATH_HOTSPOT];
        auto &visitCounter = Mark::instances[4];
        const int visitsPerPathFind = 2;
        visitCounter.currentMark += visitsPerPathFind + 1;
        auto &finishPoints = Mark::instances[3];
        ++finishPoints.currentMark;
        for (const auto &pos : destination) {
            finishPoints.justPutMark(pos);
        }
        // TODO - добавить каунтер количества раз когда посетили эту клетку
        queue.emplace(distance(entity.position) - 1, -1, entity.position);
        PathMap::pathfindMark += 5;
        int stepsCount = 0;
        while (!queue.empty()) {
            ++stepsCount;
            auto curr = queue.top();
            queue.pop();

            // если мы пришли - тут начинается жёппа - нужно восстановить путь и применить его в PathMap... :'-(
            if (finishPoints.checkMarkUnsafe(curr.coord)) {
                Vec2Int lastStep = curr.coord;
                int step = curr.step;
                int currDir = PathMap::pathfindDirection(lastStep, step);// добавить вывод конечной
                Vec2Int prevStep = curr.coord + PathMap::reverseDirection(currDir);
                PathMap::putDirection(lastStep, step, currDir);
                pathHotspot.incrementUnsafe(lastStep);
                while (step != 0) {
                    --step;

                    lastStep = prevStep;
                    pathHotspot.incrementUnsafe(lastStep);
#if(defined DEBUG_ENABLED && defined DEBUG_PATH)
                    if (pathToDraw == entity.id) {
                        MapDrawing::drawSquare(lastStep, 0x33FF0000, 6);
                    }
#endif
                    int currRestoreStepDir = PathMap::pathfindDirection(lastStep, step);
                    if (currRestoreStepDir != 4) {
                        prevStep = lastStep + PathMap::reverseDirection(currRestoreStepDir);
                    }
                    PathMap::putDirection(lastStep, step, currRestoreStepDir);
                }
#ifdef DEBUG_ENABLED
                if (prevStep != entity.position) {
                    std::cerr << "Твою ж коврижку, мимо!" << std::endl;
                    throw std::exception();
                }
#endif
                entity.nextPt = lastStep;
                return lastStep;
            }

            int currMark = std::max(visitCounter.getMarkUnsafe(curr.coord),
                                    visitCounter.currentMark - visitsPerPathFind);
            if (currMark == visitCounter.currentMark) {
                // point visited too much times
                continue;
            }
            visitCounter.putValUnsafe(curr.coord, currMark + 1);
            int maxTimeToWait = 0;
//            MapDrawing::drawSquare(curr.coord, 6);
//            std::cout << "PICK " << curr.coord.x << ' ' << curr.coord.y << " Score " << curr.priority << std::endl;
            for (const auto &currDir : semiShuffledOrder()) {
                // идём сюда-то, индекс направления currDir
                Vec2Int ngb = curr.coord + PathMap::direction(currDir);
                // не ходим на статичные препятствия
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                if (visitCounter.checkMarkUnsafe(ngb)) {
                    continue;
                }
                int timeToGo = ngb == curr.coord ? 1 : timeToGoThrough(ngb);
                if (timeToGo == 0) {
                    continue;
                }
                int newStep = curr.step + timeToGo;
                // тут нужна проверка на непересекаемость пути
                if (newStep >= PathMap::pathfindCollisionDeep) {
                    return {-1, -1};
                }
                // был ли в этой точке
                {
                    bool blocked = false;
                    for (int i = curr.step + 1; i != newStep; ++i) {
                        //dir = PathMap::directionFrom(cycleCheckPos, time);
                        // пришли ещё раз на свой путь или путь уже занят
                        if (PathMap::pathfindDirection(curr.coord, i) >= 0 ||
                            PathMap::directionFrom(curr.coord, i) >= 0) {
                            blocked = true;
                            break;
                        }
                    }
                    if (blocked) {
                        continue;
                    }
                }
                if (PathMap::pathfindDirection(ngb, newStep) >= 0) {
                    continue;
                }

                // pos, time, from
                if (!PathMap::couldGoHere(ngb, newStep, curr.coord)) {
                    continue;
                }

                // ага... тут что, только для pathRestore добавить инфу....
                int newScore = distance(ngb) + newStep;
#if(defined DEBUG_ENABLED && defined DEBUG_PATH)
                if (pathToDraw == entity.id) {
                    MapDrawing::drawSquare(ngb, 0x220000FF, 5);
                }
#endif
                if (currDir == 4) { // stay in place
                    maxTimeToWait = std::max(maxTimeToWait, 2);
                } else {
                    maxTimeToWait = std::max(maxTimeToWait, timeToGo);
                    PathMap::putPathfindDirection(ngb, newStep, currDir);
                }
                queue.emplace(newScore, newStep, ngb);
            }
            for (int i = curr.step + 1; i < curr.step + maxTimeToWait; ++i) {
                PathMap::putPathfindDirection(curr.coord, i, 4);
#if(defined DEBUG_ENABLED && defined DEBUG_PATH)
                if (pathToDraw == entity.id) {
                    MapDrawing::drawSquare(curr.coord, 0x220000FF, 5);
                }
#endif
            }
        }

        // ну что ты будешь делать :(
        return {-1, -1};
    }

    inline static Vec2Int firstStepTo(MyEntity &entity, const Vec2Int &finalPos, const int enoughDistance) {
        if (entity.position.distance(finalPos) <= enoughDistance) {
            return finalPos;
        }

        std::vector<Vec2Int> points;
        auto &marker = Mark::instances[0];
        ++marker.currentMark;
        for (int dX = 0; dX <= enoughDistance; ++dX) {
            int dY = enoughDistance - dX;
            Vec2Int newPos = finalPos + Vec2Int(dX, dY);
            if (marker.putMark(newPos)) {
                points.push_back(newPos);
            }
            Vec2Int secondPos = finalPos + Vec2Int(-dX, dY);
            if (marker.putMark(secondPos)) {
                points.push_back(secondPos);
            }
            for (int i = secondPos.x + 1; i < newPos.x; ++i) {
                marker.justPutMark({i, newPos.y});
            }
            newPos = finalPos + Vec2Int(dX, -dY);
            if (marker.putMark(newPos)) {
                points.push_back(newPos);
            }
            secondPos = finalPos + Vec2Int(-dX, -dY);
            if (marker.putMark(secondPos)) {
                points.push_back(secondPos);
            }
            for (int i = secondPos.x + 1; i < newPos.x; ++i) {
                marker.justPutMark({i, newPos.y});
            }
        }
//        std::function<int(const Vec2Int &pathfind)> distance
        return applyPathFind(entity,
                             points,
                             [&finalPos](const Vec2Int &from) { return from.distance(finalPos); });
    }

    inline static Vec2Int firstStepTo(MyEntity &entity, const MyEntity &destination, const int enoughDistance) {
        int entitySize = destination.size();
        if (entitySize == 1) {
            return firstStepTo(entity, destination.position, enoughDistance);
        }
        if (entity.distanceToEntity(&destination) <= enoughDistance) {
#ifdef DEBUG_ENABLED
            std::cerr << "firstStepTo for close entity?" << std::endl;
            throw std::exception();
#endif
            return destination.position;
        }
        auto ngbs = neighbours(destination.position, entitySize);
        auto &marker = Mark::instances[0]; // don't touch this!
        fillMark(destination.position, entitySize, marker);
        filterBorders(ngbs);
        for (const auto &pos: ngbs) {
            marker.justPutMarkUnsafe(pos);
        }
        for (int i = 1; i != enoughDistance; ++i) {
            expandIgnoreObstacles(ngbs, marker);
        }
//        MapDrawing::drawPositions(ngbs);
        return applyPathFind(entity, ngbs,
                             [position = destination.position, size = destination.size()](const Vec2Int &from) {
                                 return distanceTo(from,
                                                   position,
                                                   size);
                             });
    }

    constexpr int defaultMaxDistance = 1000;
    constexpr int64_t INF = (int64_t) (1e11);

    struct Distance {
        int resource;
        int distance;

        Distance(int resource, int distance) : resource(resource), distance(distance) {}
    };

    static void hungarian(const std::vector<int> &workerByNom,
                          std::vector<MyEntity> &entities,
                          const std::vector<std::vector<Distance>> &distances,
                          const std::vector<Vec2Int> &points,
                          const std::function<void(const Vec2Int &pt, MyEntity &entity)> &applyAim) {
        int n = distances.size();
        int m = points.size();
        bool swapped = false;
        if (n > m) {
            swapped = true;
            std::swap(n, m);
        }
        int64_t a[n + 1][m + 1];
        for (int i = 0; i <= n; ++i) {
            for (int j = 0; j <= m; ++j) {
                a[i][j] = INF;
            }
        }
        if (swapped) {
            for (int j = 0; j != m; ++j) {
                for (const auto &edge : distances[j]) {
                    a[edge.resource + 1][j + 1] = Utils::distanceScore(edge.distance);
                }
            }
        } else {
            for (int i = 0; i != n; ++i) {
                for (const auto &edge : distances[i]) {
                    a[i + 1][edge.resource + 1] = Utils::distanceScore(edge.distance);
                }
            }
        }

        std::vector<int64_t> u(n + 1), v(m + 1);
        std::vector<int> p(m + 1), way(m + 1);
        for (int i = 1; i <= n; ++i) {
            p[0] = i;
            int j0 = 0;
            std::vector<int64_t> minv(m + 1, INF);
            std::vector<char> used(m + 1, false);
            do {
                used[j0] = true;
                int i0 = p[j0];
                int64_t delta = INF, j1;
                for (int j = 1; j <= m; ++j)
                    if (!used[j]) {
                        int64_t cur = a[i0][j] - u[i0] - v[j];
                        if (cur < minv[j]) {
                            minv[j] = cur, way[j] = j0;
                        }
                        if (minv[j] < delta) {
                            delta = minv[j], j1 = j;
                        }
                    }
                for (int j = 0; j <= m; ++j)
                    if (used[j]) {
                        u[p[j]] += delta, v[j] -= delta;
                    } else {
                        minv[j] -= delta;
                    }
                j0 = j1;
            } while (p[j0] != 0);
            do {
                int j1 = way[j0];
                p[j0] = p[j1];
                j0 = j1;
            } while (j0);
        }

        std::vector<int> ans;
        if (swapped) {
            ans.swap(p);
        } else {
            ans.resize(n + 1);
            for (int j = 1; j <= m; ++j) {
                ans[p[j]] = j;
            }
        }

        for (int i = 0; i != (int) distances.size(); ++i) {
            int &resourceNom = ans[i + 1];
            if (resourceNom == 0) {
                continue;
            }
            if (swapped) {
                if (a[resourceNom][i + 1] == INF) {
                    continue;
                }
            } else {
                if (a[i + 1][resourceNom] == INF) {
                    continue;
                }
            }
            auto &aimPoint = points[resourceNom - 1];
            int workerNom = workerByNom[i];
            applyAim(aimPoint, entities[workerNom]);
        }
    }

    static void forceMoveToPoints(Mark &points,
                                  std::vector<MyEntity> &entities,
                                  const std::function<bool(const MyEntity &entity)> &checkIfBusy,
                                  const std::function<void(const Vec2Int &pt, MyEntity &entity)> &applyAim,
                                  int maxWorkersCount = 1000,
                                  int maxDistance = defaultMaxDistance) {
        auto &marker = Mark::instances[0];
#ifdef DEBUG_ENABLED
        if (&marker == &points) {
            std::cerr << "wrong markers passed" << std::endl;
            throw std::exception();
        }
#endif

        struct Queue {
            int stepCount;
            Vec2Int prevStep;

            Queue(int stepCount, const Vec2Int &prevStep) : stepCount(stepCount), prevStep(prevStep) {}
        };
        std::vector<std::vector<Distance>> worker_distance;
        worker_distance.reserve(entities.size());
        int currSearchMax = entities.size();
        std::unordered_map<Vec2Int, int> addedPoints;
        std::vector<Vec2Int> pointByNom;
        std::vector<int> workerByNom;

        auto &extendedDangerField = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        auto &staticMoveField = Mark::field[BLOCK_STATIC];

        for (int i = 0; i != (int) entities.size(); ++i) {
            const auto &builder = entities[i];
            if (checkIfBusy(builder) || extendedDangerField.nonZeroUnsafe(builder.position)) {
                continue;
            }
            auto &start = builder.position;
            ++marker.currentMark;
            std::vector<Distance> currentDistances;

            std::queue<Queue> searchQueue;
            marker.putMark(start);
            searchQueue.emplace(0, start);

            while (!searchQueue.empty()) {
                auto ref = searchQueue.front();
                searchQueue.pop();
                const auto &currPos = ref.prevStep;
                // no search for a such a long way or through endpoints
                if (ref.stepCount < maxDistance &&
                    (ref.stepCount == 0 || !points.checkMarkUnsafe(currPos) || maxDistance == defaultMaxDistance)) {
                    for (const auto &pos : currPos.neighbours()) {
                        if (Data::insideMap(pos) &&
                            !staticMoveField.checkMarkUnsafe(pos) &&
                            !extendedDangerField.nonZeroUnsafe(pos) &&
                            marker.putMarkUnsafe(pos)) {

                            searchQueue.emplace(ref.stepCount + 1, pos);
                        }
                    }
                }
                if (points.checkMarkUnsafe(currPos)) {
                    if (addedPoints.find(currPos) == addedPoints.end()) {
                        addedPoints.emplace(currPos, pointByNom.size());
                        pointByNom.push_back(currPos);
                    }
                    currentDistances.emplace_back(addedPoints[currPos], ref.stepCount);
                    if ((int) currentDistances.size() > currSearchMax) {
                        break;
                    }
                }
            }
            if (currentDistances.empty()) {
                --currSearchMax;
                continue;
            }
            worker_distance.emplace_back();
            worker_distance.rbegin()->swap(currentDistances);
            workerByNom.emplace_back(i);
        }
        while ((int) worker_distance.size() > maxWorkersCount) {
            int max = -1;
            int idx = -1;
            for (int i = (int) worker_distance.size() - 1; i >= 0; --i) {
                const auto &curr = worker_distance[i];
                int currMin = curr[0].distance;
                if (currMin > max) {
                    idx = i;
                    max = currMin;
                }
            }
            for (int i = idx; i + 1 < (int) worker_distance.size(); ++i) {
                worker_distance[i].swap(worker_distance[i + 1]);
                workerByNom[i] = workerByNom[i + 1];
            }
            worker_distance.pop_back();
            for (int i = 0; i != (int) pointByNom.size(); ++i) {
                bool found = false;
                for (const auto &wd : worker_distance) {
                    for (const auto &it : wd) {
                        if (it.resource == i) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }
                if (found) {
                    continue;
                }
                pointByNom.erase(pointByNom.begin() + i);
                for (auto &wd : worker_distance) {
                    for (auto &it : wd) {
                        if (it.resource > i) {
                            --it.resource;
                        }
                    }
                }
                --i;
            }
        }
        hungarian(workerByNom, entities, worker_distance, pointByNom, applyAim);
    }

    inline static void thr() {
#ifdef DEBUG_ENABLED
        throw std::exception();
#endif
    }

    inline static bool isSquareUnseen(const Vec2Int &pos) {
        return Mark::field[LAST_SAW].getMarkUnsafe(pos) < 0;
    }

    inline static Vec2Int findClosestForCivilianMatch(const Vec2Int &from, Mark &marker, const std::function<bool(const Vec2Int&)> &func) {
        ++marker.currentMark;
        std::queue<Vec2Int> q;
        q.emplace(from);
        marker.putMarkUnsafe(from);
        while (!q.empty()) {
            auto curr = q.front();
            q.pop();
            for (const auto &ngb : curr.neighbours()) {
                if (marker.putMark(ngb)) {
                    auto entity = Data::getUnsafe(ngb);
                    if (entity != nullptr && !entity->canMove()) {
                        continue;
                    }
                    if (func(ngb)) {
                        return ngb;
                    }
                    q.emplace(ngb);
                }
            }
        }
        return {-1, -1};
    }
}

#endif //AICUP2020_UTILS_H
