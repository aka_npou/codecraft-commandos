//
// Created by dragoon on 29.11.2020.
//

#ifndef AICUP2020_MYACTION_H
#define AICUP2020_MYACTION_H

#include "../model/Vec2Int.hpp"
#include "../model/EntityType.hpp"

enum ActionType {
    MOVE_TO,
    ATTACK_TARGET,
    BUILD,
    DIG,
    REPAIR,
    RETREAT
};

struct MyAction {
    ActionType actionType;
    Vec2Int target;
    EntityType entityType;

    MyAction(ActionType actionType, const Vec2Int &target);

    MyAction(ActionType actionType, const Vec2Int &target, EntityType entityType);
};


#endif //AICUP2020_MYACTION_H
