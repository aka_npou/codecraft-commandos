//
// Created by dragoon on 29.11.2020.
//

#include "MyAction.h"

MyAction::MyAction(ActionType actionType, const Vec2Int &target)
        : actionType(actionType), target(target), entityType(UNKNOWN) {}

MyAction::MyAction(ActionType actionType, const Vec2Int &target, EntityType entityType)
        : actionType(actionType), target(target), entityType(entityType) {}
