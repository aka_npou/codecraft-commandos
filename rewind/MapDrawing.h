#pragma once

//
// Created by dragoon on 28.11.2020.
//

#include <string>
#include <algorithm>
#include <sstream>
#include "RewindClient.h"
#include "../myModel/Data.h"
#include "../myModel/PlayerData.h"
#include "../myModel/Mark.h"

namespace MapDrawing {

    const uint32_t players_colors[4] = {0xFF0000, 0xFF00FF, 0x0000FF, 0xFFFF00};
    const uint32_t my_color = 0x00FF00;


    const uint32_t entity_colors[10] = {
            0xAAAA00, //0
            0xAA00AA, //1
            0x009900, //2
            0x009900, //3
            0x00AAFF, //4
            0x00AAFF, //5
            0x000000, //6
            0x000000, //7
            0xFFFFFF, //8
            0xFF0000  //9
    };

    static void addAlphaHpBased(uint32_t &colour, int currHp, int maxHp) {
        double percentage = (currHp / (double) maxHp) * 0.8 + 0.2;
        int newAlpha = (int) (255 * percentage);
        colour |= newAlpha << 24;
    }

    static void drawSquare(const Vec2Int &pos, uint32_t color, int layer = 8) {
        RewindClient &draw = RewindClient::instance();
        draw.set_options(layer, false);
        draw.rectangle(pos.x, pos.y, pos.x + 1, pos.y + 1, color, true);
    }

    static void drawSquare(const Vec2Int &pos, int layer = 8) {
        drawSquare(pos, 0x550000FF, layer);
    }

    static void drawPositions(const std::vector<Vec2Int> &points, int layer = 8) {
        for (const auto &pos : points) {
            drawSquare(pos, layer);
        }
    }

    static double healthFromHp(double x1, double x2, int health, int maxHealth) {
        return x1 + (x2 - x1) * (health / (double) maxHealth);
    }

    static void drawEntity(const MyEntity &entity, const PlayerView &playerView, const uint32_t *playerColors) {
        RewindClient &draw = RewindClient::instance();

        const auto &props = playerView.entityProperties.at(entity.entityType);
        double x1 = entity.position.x;
        double y1 = entity.position.y;
        double x2 = entity.position.x + props.size;
        double y2 = entity.position.y + props.size;
        double mx = (x1 + x2) / 2.;
        double my = (y1 + y2) / 2.;
        draw.set_options(4);
        if (entity.playerId || entity.health < props.maxHealth) {
            std::stringstream sstr;
            if (entity.playerId) {
                sstr << entity.playerId << "\\n" << entity.id;
            }
            if (entity.health < props.maxHealth) {
                if (entity.playerId) {
                    sstr << "\\n";
                }
                sstr << entity.health;
            }
            draw.popup(x1, y1, x2, y2, sstr.str());
            if (entity.health < props.maxHealth) {
                draw.set_options(5);
                draw.rectangle(x1, y2 - 0.1, x2, y2, 0x000000, true);
                draw.rectangle(x1, y2 - 0.1, healthFromHp(x1, x2, entity.health, props.maxHealth), y2, 0xFF0000, true);
                draw.set_options(4);
            }
        }
        if (!entity.playerId) {
            int color = (255 * entity.health) / props.maxHealth;
            color = color | (color << 8) | (color << 16);
            draw.rectangle(x1, y1, x2, y2, color, true);
            return;
        }
        uint32_t color = playerColors[(int) entity.playerId];
        if (!entity.active) {
            addAlphaHpBased(color, entity.health, props.maxHealth);
        }
        draw.rectangle(x1, y1, x2, y2, color, true);
        x1 = (x1 + mx) / 2.;
        x2 = (x2 + mx) / 2.;
        y1 = (y1 + my) / 2.;
        y2 = (y2 + my) / 2.;

        color = entity_colors[entity.entityType];
        if (!entity.active) {
            addAlphaHpBased(color, entity.health, props.maxHealth);
        }
        draw.rectangle(x1, y1, x2, y2, color, true);
    }

    static void drawMap(const PlayerView &playerView, PlayerData &me, std::vector<PlayerData> &others) {
        RewindClient &draw = RewindClient::instance();
        draw.set_options(4, false);

        uint32_t colors[playerView.players.size() + 1];
        for (int i = 1; i <= (int) playerView.players.size(); ++i) {
            colors[i] = players_colors[i - 1];
        }
        colors[playerView.myId] = my_color;

        for (int x = 0; x != Data::mapSize; ++x) {
            for (int y = 0; y != Data::mapSize; ++y) {
                auto const entityPtr = Data::get(x, y);
                if (entityPtr == nullptr) {
                    continue;
                }
                auto const &entity = *entityPtr;
                if (!entity.position.match(x, y)) {
                    continue;
                }
                drawEntity(entity, playerView, colors);
            }
        }
        std::vector<Player> playersCopy = playerView.players;
        draw.message("tick %d", playerView.currentTick);
        draw.message("%d: %d %d %d/%d (me)",
                     me.id(),
                     me.player.resource,
                     me.player.score,
                     me.totalUnits,
                     me.totalHouses);
        for (const auto &player : others) {
            draw.message("%d: %d %d %d/%d %d %d %s",
                         player.id(),
                         player.player.resource,
                         player.player.score,
                         player.totalUnits,
                         player.totalHouses,
                         player.dRes,
                         player.dScore,
                         player.startedBarrack ? "+" : "-");
        }
        draw.message("Heal counter: %d", Data::healCounter);
    }

    static void drawFromTo(const MyEntity &from, const Vec2Int &to, uint32_t color) {
        RewindClient &draw = RewindClient::instance();
        auto fromPt = from.center();
        auto toPt = Vec2Double(to.x + .5, to.y + .5);
        draw.line(fromPt.x, fromPt.y, toPt.x, toPt.y, color);
    }

    static void drawEntityAction(const std::vector<MyEntity> &src) {
        RewindClient &draw = RewindClient::instance();
        for (const auto &entity: src) {
            if (!entity.action) {
                continue;
            }
            auto &action = *entity.action;

            switch (action.actionType) {
                case REPAIR:
                    draw.set_options(6);
                    drawFromTo(entity, entity.action->target, 0x008800);
                    break;
                case MOVE_TO:
                    draw.set_options(7);
                    drawFromTo(entity, entity.action->target, 0x000000);
                    break;
                case ATTACK_TARGET:
                    draw.set_options(8);
                    drawFromTo(entity, entity.action->target, 0xAA0000);
                    break;
                case BUILD:
                    draw.set_options(9);
                    drawFromTo(entity, entity.action->target, 0xAAAA00);
                    break;
                case DIG:
                    if (!(entity.position == entity.action->target)) {
                        draw.set_options(10);
                        drawFromTo(entity, entity.action->target, 0x8E6C4A);
                    }
                    break;
            }
        }
    }

    inline static int getColor(int min, int mid, int max, int val) {
        if (val >= mid) {
            return (((int) (((max - val) / (double) (max - mid)) * 255)) << 16) | (255 << 8);
        } else {
            return (((int) (((val - min) / (double) (mid - min)) * 255)) << 8) | (255 << 16);
        }
    }

    inline static int getColor(int min, int max, int val) {
        int mid = (max - min) >> 1;
        return getColor(min, mid, max, val);
    }

    inline static uint32_t applyAlpha(int max, int val, uint32_t baseColor) {
        return baseColor | (static_cast<int>((255. / max) * val) << 24);
    }

    static void drawOverlapField(const Mark &mark, uint32_t baseColor, int layer = 2) {
        int max = 0;
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.mark[i][j] != std::numeric_limits<int>::max() && max < mark.mark[i][j]) {
                    max = mark.mark[i][j];
                }
            }
        }
        ++max;
        auto &draw = RewindClient::instance();
        draw.set_options(layer, false);
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.mark[i][j] != 0) {
                    draw.rectangle(i, j, i + 1, j + 1, applyAlpha(max, mark.mark[i][j], baseColor), true);
                }
            }
        }
    }

    static void drawGradientField(const Mark &mark,
                                  int limitNotForCalc = std::numeric_limits<int>::max(),
                                  int layer = 2) {
        int max = 0;
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.mark[i][j] != limitNotForCalc && max < mark.mark[i][j]) {
                    max = mark.mark[i][j];
                }
            }
        }
        auto &draw = RewindClient::instance();
        draw.set_options(layer, false);
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.mark[i][j] != limitNotForCalc) {
                    draw.rectangle(i, j, i + 1, j + 1, getColor(0, max, mark.mark[i][j]) | 0x88000000, true);
                }
            }
        }
    }

    static void drawGradientWithMidField(const Mark &mark, int layer = 2) {
        int max = std::numeric_limits<int>::min();
        int min = std::numeric_limits<int>::max();
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                max = std::max(max, mark.mark[i][j]);
                min = std::min(min, mark.mark[i][j]);
            }
        }
        auto &draw = RewindClient::instance();
        draw.set_options(layer, false);
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                draw.rectangle(i, j, i + 1, j + 1, getColor(min, 0, max, mark.mark[i][j]) | 0x88000000L, true);
            }
        }
    }

    static void drawYesNoField(const Mark &mark, uint32_t baseColor = 0, int layer = 4, bool inverted = false) {
        auto &draw = RewindClient::instance();
        draw.set_options(layer, false);
        for (int i = 0; i != Data::mapSize; ++i) {
            int firstOk = 999;
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.checkMarkUnsafe({i, j}) ^ inverted) {
                    if (firstOk > j) {
                        firstOk = j;
                    }
                } else if (firstOk < j) {
                    draw.rectangle(i, firstOk, i + 1, j, baseColor, true);
                    firstOk = 999;
                }
            }
            if (firstOk != 999) {
                draw.rectangle(i, firstOk, i + 1, Data::mapSize, baseColor, true);
            }
        }
    }
}