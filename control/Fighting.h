//
// Created by dragoon on 17.12.2020.
//

#ifndef AICUP2020_FIGHTING_H
#define AICUP2020_FIGHTING_H

#include <set>
#include <limits.h>

namespace Fighting {

    constexpr int TIME_TO_HOLD_POSITION = 10;
    constexpr int MAX_DIST_TO_WORKER = 30;

    inline static MyEntity *findClosestEntity(const Vec2Int &startingPos, int damage, const std::function<bool(const MyEntity* entity)> &check) {
        auto &marker = Mark::instances[0];
        auto &staticMarker = Mark::field[BLOCK_STATIC];
        ++marker.currentMark;
        struct PQ {
            int steps;
            Vec2Int pos;

            PQ(int steps, const Vec2Int &pos) : steps(steps), pos(pos) {}

            bool operator<(const PQ &other) const {
                return this->steps > other.steps;
            }
        };
        std::priority_queue<PQ> queue;
        marker.putMarkUnsafe(startingPos);
        queue.emplace(0, startingPos);
        while (!queue.empty()) {
            auto curr = queue.top();
            queue.pop();

            for (const auto &ngb : curr.pos.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto entity = Data::getUnsafe(ngb);
                if (check(entity)) {
                    return entity;
                } else {
                    if (!marker.putMarkUnsafe(ngb)) {
                        continue;
                    }

                    if (entity == nullptr || entity->canMove()) {
                        // not an obstacle
                        queue.emplace(curr.steps + 1, ngb);
                    } else if (entity->entityType == RESOURCE) {
                        queue.emplace(curr.steps + (entity->health + damage - 1) / damage + 1,
                                      ngb); // time to break through
                    }
                    // это здание. Обходим здания
                }
            }
        }
        return nullptr;
    }

    inline static bool isMyBuilder(const MyEntity *entity) {
        return entity != nullptr && entity->playerId == Data::myId && entity->entityType == BUILDER_UNIT;
    }

    inline static MyEntity *findClosestBuilder(const Vec2Int &startingPos, int damage) {
        return findClosestEntity(startingPos, damage, isMyBuilder);
    }

    inline static MyEntity *findClosestEnemy(const Vec2Int &startingPos, int damage) {
        return findClosestEntity(startingPos, damage, Utils::isEnemy);
    }

    static MyEntity *chooseBestTarget(int attackDmg, const std::vector<MyEntity *> &enemies) {
        MyEntity *bestEntity;
        int bestScore = 0;
        for (auto entity: enemies) {
            int maxScore;
            switch (entity->entityType) {
                case RANGED_UNIT:
                case MELEE_UNIT:
                    maxScore = 10000;
                    break;
                case TURRET:
                    maxScore = 30000;
                    break;
                case BUILDER_UNIT:
                    maxScore = 5000;
                    break;
                default:
                    maxScore = 2500;
            }
            int score;
            int currHp = entity->health - entity->actionsCount;
            int hitsToKill = (currHp + attackDmg - 1) / attackDmg;
            if (hitsToKill <= 0) {
                score = 1;
            } else {
                score = maxScore / hitsToKill;
            }
            if (score > bestScore) {
                bestScore = score;
                bestEntity = entity;
            }
        }
        return bestEntity;
    }

//    inline void addToFrom(std::unordered_map<int, std::vector<int>> &reachable,
//                          std::unordered_map<int, std::vector<int>> &back_reachable,
//                          int myId,
//                          int enemyId) {
//        auto val = reachable.emplace(myId, std::vector<int>());
//        val.first->second.emplace_back(enemyId);
//        val = back_reachable.emplace(enemyId, std::vector<int>());
//        val.first->second.emplace_back(myId);
//    }

    // TODO: replace with hold position?
    inline static void stayFor(MyEntity &entity, int time) {
        if (entity.moved()) {
            Utils::removePath(entity);
        }
        entity.nextPt = entity.position;
        for (int i = 0; i != time; ++i) {
            int currDir = PathMap::directionFrom(entity.position, i);
            if (currDir < 0) {
                PathMap::putDirection(entity.position, i, 4);
            } else {
//                Utils::thr();
                break;
            }
        }
    }

    inline static void cleanUnitStep(const Vec2Int &pos,
                                     int time,
                                     Mark &positions,
                                     std::unordered_multimap<int, Vec2Int> &blocked_points) {
        int currDir = PathMap::directionFrom(pos, time);
        if (currDir < 0) {
            return;
        }
        auto blockUnit = Utils::findUnit(pos, time);
        blocked_points.emplace(blockUnit->id, pos);
        blockUnit->state = -1;
        // чистим тут... бедолаге надо попытаться снова
        positions.decrementUnsafe(Utils::getLastStepForTime(*blockUnit, 0).first);
        Utils::removePath(*blockUnit);
#ifdef DEBUG_ENABLED
        std::cout << "Cancel path for unit id " << blockUnit->id <<
                  " x:" << blockUnit->position.x <<
                  " y:" << blockUnit->position.y <<
                  std::endl;
#endif
    }

    inline static void holdPosition(MyEntity &entity,
                                    int time,
                                    Mark &positions,
                                    std::unordered_multimap<int, Vec2Int> &blocked_points) {
        Vec2Int nextPoint = Utils::getLastStepForTime(entity, 0).first;
        positions.putMarkUnsafe(nextPoint); // столбим эту точку себе
        entity.nextPt = nextPoint; // Если мы сказали HOLD и time == 0 - надо обновить тут

        auto pair = Utils::getLastStepForTime(entity, time);
        for (int i = pair.second + 1; i < time; ++i) {
            cleanUnitStep(pair.first, i, positions, blocked_points);
            PathMap::putDirection(pair.first, i, 4);
        }
    }

    bool shootResources(MyEntity &entity, EntityAction &entityAction) {
        // Применимо если стою на месте или иду в своего, который "занят" (рабочий копатель, ремонтник)
        if (entity.moved() && entity.position != entity.nextPt) {
            auto nextEntity = Data::getUnsafe(entity.nextPt);
            if (nextEntity == nullptr || nextEntity->moved()) {
                return false;
            }
        }
        MyEntity *resourceToAttack = nullptr;
        int score = -50000000;
        Utils::applyForAllCoordsInRange(
                entity.attackRange(),
                entity.position,
                [&entity, &resourceToAttack, &score](const Vec2Int &pos) {
                    if (Mark::field[PATH_HOTSPOT].getMarkUnsafe(pos) == 0) {
                        return;
                    }

                    auto newEntity = Data::getUnsafe(pos);
                    if (newEntity == nullptr || newEntity->entityType != RESOURCE) {
                        return;
                    }
                    int newScore = (newEntity->actionsCount % 5 == 0) ? 10 : 0;
                    newScore += 10 - entity.position.distance(pos);
                    int remains = newEntity->health - newEntity->actionsCount;
                    if (remains <= 0) {
                        return;
                    }
                    if (remains % 5 == 0) {
                        newScore += 5;
                    }
                    newScore += ((30 - remains + 4) / 5) * 20;
                    newScore -= Mark::field[ENEMY_CIVILIAN_DISTANCE_FIELD].getMarkUnsafe(pos) * 5;
                    if (newScore > score) {
                        score = newScore;
                        resourceToAttack = newEntity;
                    }
                });
        if (resourceToAttack != nullptr) {
            entity.action = std::make_unique<MyAction>(DIG, resourceToAttack->position);
            resourceToAttack->actionsCount += entity.attackDamage();
            entityAction.attackAction =
                    std::make_shared<AttackAction>(std::make_shared<int>(resourceToAttack->id), nullptr);
            stayFor(entity, 5);
            return true;
        }
        return false;
    }

    inline static int getOneDir(const Vec2Int &fromPos, const Vec2Int &direction, const Mark &myPositions) {
        auto &enemyMoved = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        int startPoint = enemyMoved.getMarkUnsafe(fromPos);
        Vec2Int pos = fromPos;
        int skipped = 0;
        int nextScore = startPoint - 1;
        int idx = 0;
        while (true) {
            pos = pos + direction;
            if (!myPositions.checkMark(pos)) {
                if (++skipped >= 1) {
                    break;
                }
                continue;
            }
            int nextMark = enemyMoved.getMarkUnsafe(pos);
            if (nextMark >= nextScore) {
                ++idx;
                if (idx % 2 == 1) {
                    --nextScore;
                }
            }
        }
        return idx;
    }

    inline static bool checkIfEverythingOk(const Vec2Int &fromPos, const Vec2Int &direction, const Mark &myPositions) {
        int sum = getOneDir(fromPos, direction, myPositions);
        sum += getOneDir(fromPos, -direction, myPositions);
        auto &enemyMoved = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        int startPoint = enemyMoved.getMarkUnsafe(fromPos);
        return sum + 1 >= startPoint;
    }

    inline static bool checkIfBlocked(const std::unordered_multimap<int, Vec2Int> &blockedPoints,
                                      int unitId,
                                      const Vec2Int &pos) {
        auto iters = blockedPoints.equal_range(unitId);
        while (iters.first != iters.second) {
            if (iters.first->second == pos) {
                return true;
            }
            ++iters.first;
        }
        return false;
    }

    inline static void applyArchersAttack(std::vector<MyEntity> &entities,
                                          std::unordered_map<int, EntityAction> &entityActions) {
        if (entities.empty()) {
            return;
        }
        auto &enemyDangerField = Mark::field[ENEMY_DANGER_FIELD];
        auto &myWorkersDistance = Mark::field[MY_WORKERS_DISTANCE_FIELD];

        auto &entityProps = MyEntity::props[entities[0].entityType];
        auto attackRange = entityProps.attack->attackRange;
        auto attackDmg = entityProps.attack->damage;
        std::vector<int> couldAttack;
        struct FarestSort {
            int distance;
            int idx;
            MyEntity *target;

            FarestSort(int distance, int idx, MyEntity *target) : distance(distance), idx(idx), target(target) {}
        };
        std::vector<FarestSort> currProcess;
        for (int idx = 0; idx != entities.size(); ++idx) {
            auto &unit = entities[idx];
            EntityAction &entityAction = (entityActions.insert(std::make_pair(unit.id, EntityAction())).first->second);
            if (Utils::hasEnemiesInAttackRange(attackRange, unit)) {
                couldAttack.push_back(idx);
                continue;
            }

            // такс, проверяем что мы не под атакой и набочий не оч далеко
            // о, ещё ж хп то проверить...
            if (unit.actionsCount + unit.health + 5 <= unit.maxHealth() &&
                enemyDangerField.getMarkUnsafe(unit.position) == 0 &&
                myWorkersDistance.getMarkUnsafe(unit.position) <= MAX_DIST_TO_WORKER) {

                auto builder = findClosestBuilder(unit.position, attackDmg);
                currProcess.emplace_back(Mark::field[ENEMY_CIVILIAN_DISTANCE_FIELD].getMarkUnsafe(unit.position),
                                         idx,
                                         builder); // приоритет хода - ходит первым
                continue;
            }

            auto closestEnemy = findClosestEnemy(unit.position, attackDmg); // Какого чёрта ищем вокруг...
            if (closestEnemy == nullptr) { // на карте не найдено целей...
                entityAction.moveAction = std::make_shared<MoveAction>(Vec2Int(40, 40), false, false);
                continue;
            }
            int distance = closestEnemy->position.distance(unit.position);
#ifdef DEBUG_ENABLED
            if (distance <= attackRange) {
                std::cerr << "Impossible!" << std::endl;
                throw std::exception();
            } else
#endif
            {
                currProcess.emplace_back((distance - attackRange) * 1000 +
                                         Mark::field[ENEMY_CIVILIAN_DISTANCE_FIELD].getMarkUnsafe(unit.position),
                                         idx,
                                         closestEnemy);
            }
        }
        TimeMeasure::end(8);

        auto &positions = Mark::instances[2];
        ++positions.currentMark;

        for (const auto &idx : couldAttack) { // TODO: тут надо добавить убегание от мечников
            auto &unit = entities[idx];
            positions.justPutMarkUnsafe(unit.position);
            std::vector<MyEntity *> enemies = Utils::getAchievableEnemies(attackRange, unit.position);
            auto target = chooseBestTarget(attackDmg, enemies);
            target->actionsCount += attackDmg;
            entityActions[unit.id].attackAction =
                    std::make_shared<AttackAction>(std::make_shared<int>(target->id), nullptr);
            unit.action = std::make_unique<MyAction>(ATTACK_TARGET, target->position);
            stayFor(unit, 10);
        }

        std::sort(currProcess.begin(),
                  currProcess.end(),
                  [&entities](const FarestSort &first, const FarestSort &second) {
                      if (first.distance == second.distance) {
                          return entities[first.idx].position < entities[second.idx].position;
                      } else {
                          return first.distance < second.distance;
                      }
                  });
        auto &enemyDirect = Mark::field[ENEMY_DANGER_FIELD];
        auto &enemyMoved = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        auto &movedField2 = Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2];

        std::unordered_multimap<int, Vec2Int> blockedPoints;
        for (const auto &elem : currProcess) {
            auto &unit = entities[elem.idx];
            int range = elem.target->playerId == unit.playerId ? 1 : attackRange;
            Utils::firstStepTo(unit, *elem.target, range);
            holdPosition(unit, TIME_TO_HOLD_POSITION, positions, blockedPoints);
        }
//        nextStepProcess.swap(currProcess);
//        nextStepProcess.clear();
        int changes = 1;
        int retries = 0;
        while (changes != 0 && retries <= 20) {
            ++retries;
            changes = 0;
            for (const auto &elem : currProcess) {
                auto &unit = entities[elem.idx];
                if (unit.state == -1) {
                    ++changes;
                    int range = elem.target->playerId == unit.playerId ? 1 : attackRange;
                    Utils::firstStepTo(unit, *elem.target, range);
                    holdPosition(unit, TIME_TO_HOLD_POSITION, positions, blockedPoints);
                    unit.state = 0;
                }
                // При отмене не забываем удалять
                Vec2Int nextPos = Utils::getLastStepForTime(unit, 0).first;
                Vec2Int newPos;
                int nextPosEnemies = enemyMoved.getMarkUnsafe(nextPos);
                if (nextPosEnemies == 0) { // no need in changes
                    continue;
                }
                bool check = true;
                if (nextPosEnemies == 1) {
                    // проверить, что мб я сильнее и стоит ли соваться
                    // сначала может там милишник
                    auto enemies = Utils::getAchievableEnemies(2, nextPos);
                    for (const auto &enemy : enemies) {
                        if (enemy->entityType == MELEE_UNIT) {
                            check = false;
                            break;
                        }
                    }
                    enemies = Utils::getAchievableEnemies(6, nextPos);
                    Utils::eraseMatched<MyEntity *>(enemies, [](const MyEntity *enemy) {
                        return enemy->entityType != RANGED_UNIT;
                    });
                    if (enemies.size() > 1) {
                        check = false;
                    } else {
                        int hitsToKillMe = (unit.health + 4) / 5;
                        int hitsToKillHim = (enemies[0]->health + 4) / 5;
                        if (hitsToKillMe < hitsToKillHim) {
                            check = false;
                        } else {
                            if (hitsToKillHim == hitsToKillMe) {
                                // Если расстояние до его рабочих меньше - бяжим далей
                                if (Mark::field[MY_WORKERS_DISTANCE_FIELD].getMarkUnsafe(nextPos) >
                                    Mark::field[ENEMY_WORKERS_DISTANCE_FIELD].getMarkUnsafe(nextPos)) {
                                    int allies = 0;
                                    Utils::applyForAllCoordsInRange(7, enemies[0]->position,
                                                             [&allies](const Vec2Int &pos) {
                                                auto entity = Data::getUnsafe(pos);
                                                if (entity != nullptr && entity->playerId == Data::myId && entity->entityType == RANGED_UNIT) {
                                                    ++allies;
                                                }
                                            });
                                    if (allies <= 1) {
                                        check = false;
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (check) {
                    check = checkIfEverythingOk(nextPos, {1, 1}, positions);
                    if (!check) {
                        check = checkIfEverythingOk(nextPos, {1, -1}, positions);
                    }
                }
                if (!check) { // Мммм... Для начала вернёмся на свою точку, если и так не выйдет - над сбягать
                    // Надо понять - сейчас атака или мы уже пытались "постоять"? Хм.
                    if (unit.state == 0) { // угу. значит мы в атаке
                        unit.state = 1;
                        if (nextPos != unit.position && !checkIfBlocked(blockedPoints, unit.id, unit.position)) {
                            positions.decrementUnsafe(nextPos);
                            Utils::removePath(unit); // Отмена движения. Надо проверить, не хотел ли кто-то стать на нашу клетку
                            newPos = Utils::getLastStepForTime(unit, 0).first;
                            positions.justPutMarkUnsafe(newPos);
//                            unit.state = 1;
                            if (newPos != nextPos) {
                                ++changes;
                                holdPosition(unit, TIME_TO_HOLD_POSITION, positions, blockedPoints);
                                continue;
                            } else {
#ifdef DEBUG_ENABLED
                                std::cerr << "Ёпс" << std::endl;
                                throw std::exception();
#endif
                            }
                        }
                    }
                    if (unit.state == 1) { // пытаемся найти свободную самую безопасную свободную
                        unit.state = 2;
                        positions.decrementUnsafe(nextPos);
                        if (unit.moved()) {
                            Utils::removePath(unit);
                        }
                        Vec2Int bestPos;
                        int bestScore = INT_MAX;
                        Utils::forMeAndNeighbours(unit.position,
                                                  [&bestPos, &bestScore, &enemyMoved, &blockedPoints, id = unit.id](
                                                          const Vec2Int &checkPos) {
                                                      // тут свободно. Однако мы можем закольцеваться. Ну пока забьём
                                                      if (PathMap::directionFrom(checkPos, 0) >= 0) {
                                                          return;
                                                      }
                                                      if (checkIfBlocked(blockedPoints, id, checkPos)) {
                                                          return;
                                                      }

                                                      int mark = enemyMoved.getMarkUnsafe(checkPos);
                                                      if (mark < bestScore) {
                                                          bestScore = mark;
                                                          bestPos = checkPos;
                                                      }
                                                  });
                        if (bestScore != INT_MAX && bestPos != nextPos) {
                            ++changes;
                            Utils::firstStepTo(unit, bestPos, 0);
                            newPos = Utils::getLastStepForTime(unit, 0).first;
                            positions.justPutMarkUnsafe(newPos);
                            holdPosition(unit, TIME_TO_HOLD_POSITION, positions, blockedPoints);
                            continue;
                        }
                        positions.justPutMarkUnsafe(nextPos); // ничего не нашли - вернули "всё как было"
                    }
                    // возможно этот стейт лишний
                    if (unit.state == 2) { // пытаемся найти свободную самую безопасную
                        unit.state = 3;
                        positions.decrementUnsafe(nextPos);
                        if (unit.moved()) {
                            Utils::removePath(unit);
                        }
                        Vec2Int bestPos;
                        int bestScore = INT_MAX;
                        Utils::forMeAndNeighbours(unit.position,
                                                  [&bestPos, &bestScore, &enemyMoved, &blockedPoints, id = unit.id](
                                                          const Vec2Int &checkPos) {
                                                      if (checkIfBlocked(blockedPoints, id, checkPos)) {
                                                          return;
                                                      }
                                                      auto entity = Data::getUnsafe(checkPos);
                                                      if (entity != nullptr) {
                                                          if (entity->entityType != RANGED_UNIT &&
                                                              entity->entityType != MELEE_UNIT) {
                                                              return;
                                                          }
                                                          if (entity->action &&
                                                              entity->action->actionType == ATTACK_TARGET) {
                                                              return;
                                                          }
                                                      }
                                                      // тут свободно. Однако мы можем закольцеваться. Ну пока забьём
                                                      int mark = enemyMoved.getMarkUnsafe(checkPos);
                                                      if (mark < bestScore) {
                                                          bestScore = mark;
                                                          bestPos = checkPos;
                                                      }
                                                  });


                        if (bestScore == INT_MAX) {
#ifdef DEBUG_ENABLED
                            std::cerr << "Дожили. Пипец сутолока, ну не толпитесь ребят..." << std::endl;
#endif
                            bestPos = unit.position; // ignore blocks.
                        }
                        ++changes;
                        cleanUnitStep(bestPos, 0, positions, blockedPoints);
                        Utils::firstStepTo(unit, bestPos, 0);
                        newPos = Utils::getLastStepForTime(unit, 0).first;
                        positions.justPutMarkUnsafe(newPos);
                        holdPosition(unit, TIME_TO_HOLD_POSITION, positions, blockedPoints);
                    }
                }
            }
        }
#if (defined(DEBUG_ENABLED) && defined(REWIND_VIEWER))
        Data::maxRetries = std::max(Data::maxRetries, retries);
        RewindClient::instance().message("Retries for archers: %d, MR: %d", retries, Data::maxRetries);
#endif

        for (const auto &elem : currProcess) {
            auto &unit = entities[elem.idx];
            auto &ea = entityActions[unit.id];
            if (shootResources(unit, ea)) {
                continue;
            }
            if (unit.moved()) {
                ea.moveAction = std::make_shared<MoveAction>(unit.nextPt, false, false);
            } else {
                ea.clean();
            }
        }
    }

    // only melee units...
    inline static void applyAttackActions(std::vector<MyEntity> &entities,
                                          std::unordered_map<int, EntityAction> &entityActions) {
        if (entities.empty()) {
            return;
        }
        auto &entityProps = MyEntity::props[entities[0].entityType];
        auto attackRange = entityProps.attack->attackRange;
        auto attackDmg = entityProps.attack->damage;
        for (auto &unit : entities) {
            EntityAction &entityAction = (entityActions.insert(std::make_pair(unit.id, EntityAction())).first->second);
            std::vector<MyEntity *> enemies = Utils::getAchievableEnemies(attackRange, unit);
            if (!enemies.empty()) {
                auto target = chooseBestTarget(attackDmg, enemies);
                target->actionsCount += attackDmg;
                entityAction.attackAction = std::make_shared<AttackAction>(std::make_shared<int>(target->id), nullptr);
                unit.action = std::make_unique<MyAction>(ATTACK_TARGET, target->position);
                continue;
            }
            auto closestEnemy = findClosestEnemy(unit.position, attackDmg);
            if (closestEnemy == nullptr) {
#ifdef DEBUG_ENABLED
                std::cerr << "Can't find enemy!" << std::endl;
                throw std::exception();
#endif
                entityAction.moveAction = std::make_shared<MoveAction>(Vec2Int(40, 40), false, false);
                continue;
            }
            Vec2Int nextStep = Utils::firstStepTo(unit, *closestEnemy, attackRange);
            if (nextStep.x < 0) {
#ifdef DEBUG_ENABLED
                std::cerr << "Path not found for attack" << std::endl;
#endif
                entityAction.moveAction = std::make_shared<MoveAction>(closestEnemy->position, true, false);
                continue;
            } else {
                entityAction.moveAction = std::make_shared<MoveAction>(nextStep, false, false);
            }
        }
    }

    inline static void applyTurretActions(std::vector<MyEntity> &entities,
                                          std::unordered_map<int, EntityAction> &entityActions) {
        auto &turretProps = MyEntity::props[TURRET];
        auto attackRange = turretProps.attack->attackRange;
        auto attackDmg = turretProps.attack->damage;
        for (auto &unit : entities) {
            EntityAction &entityAction = (entityActions.insert(std::make_pair(unit.id, EntityAction())).first->second);
            std::vector<MyEntity *> enemies = Utils::getAchievableEnemies(attackRange, unit);
            if (enemies.empty()) {
                continue;
            }
            auto target = chooseBestTarget(attackDmg, enemies);
            target->actionsCount += attackDmg;
            entityAction.attackAction = std::make_shared<AttackAction>(std::make_shared<int>(target->id), nullptr);
            unit.action = std::make_unique<MyAction>(ATTACK_TARGET, target->position);
        }
    }

    inline static std::pair<int, int> rearrangeAttacks(std::vector<MyEntity *> &units,
                                                       std::unordered_map<int, EntityAction> &entityActions) {
        int newKills = 0;
        int rearranged = 0;
        for (auto &unit : units) {
            std::vector<MyEntity *> enemies = Utils::getAchievableEnemies(unit->attackRange(), *unit);
            if (enemies.empty()) {
                std::cerr << "UNIT NOT FOUND TARGET" << std::endl;
                throw std::exception();
            } else {
                auto oldTarget = Data::getUnsafe(unit->action->target);
                oldTarget->actionsCount -= unit->attackDamage();
                auto newTarget = chooseBestTarget(unit->attackDamage(), enemies);
                if (newTarget->position == unit->action->target) {//nothing changed
                    oldTarget->actionsCount += unit->attackDamage();
                    continue;
                }

                int prevDamage =
                        std::min(unit->attackDamage(), std::max(oldTarget->health - oldTarget->actionsCount, 0));
                int newDamage =
                        std::min(unit->attackDamage(), std::max(newTarget->health - newTarget->actionsCount, 0));
                rearranged += newDamage - prevDamage;
//                if (newDamage == 0) {
//                    continue;
//                }
                int newKill = 0;
                if (oldTarget->health > oldTarget->actionsCount &&
                    oldTarget->health - oldTarget->actionsCount <= unit->attackDamage()) {
                    --newKill;
                }
                if (newTarget->health > newTarget->actionsCount &&
                    newTarget->health - newTarget->actionsCount <= unit->attackDamage()) {
                    ++newKill;
                }
                newTarget->actionsCount += unit->attackDamage();
                newKills += newKill;

                entityActions[unit->id].attackAction->target = std::make_shared<int>(newTarget->id);
                unit->action->target = newTarget->position;
            }
        }
#ifdef DEBUG_ENABLED
        std::cout << "New Damage " << rearranged << " new Kills " << newKills << std::endl;
#endif
        return std::make_pair(rearranged, newKills);
    }
}
#endif //AICUP2020_FIGHTING_H
