//
// Created by dragoon on 01.12.2020.
//

#ifndef AICUP2020_RESOURCECOLLECTION_H
#define AICUP2020_RESOURCECOLLECTION_H

#include "../myModel/PlayerData.h"

namespace ResourceCollection {

    constexpr int IGNORE_RESOURCE_DIGGING_IN_HOTSPOTS_KOEFF = 5;

    // distanceScore firstStep aimEntity
    static void find_best_dig_pairs(PlayerData &me, const std::vector<MyEntity> &resources) {
        auto &foodDistanceField = Mark::field[FOOD_DISTANCE];
        foodDistanceField.fill(std::numeric_limits<int>::max());
        Data::expectedResource = 0;

        auto &resourcePtrMarker = Mark::field[RESOURCE_DIG_MARK];
        auto &extendedDangerField = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        auto &pathHotspot = Mark::field[LAST_PATH_HOTSPOT];
        auto &workersDistanceField = Mark::field[MY_WORKERS_DISTANCE_FIELD];
        ++resourcePtrMarker.currentMark;
        for (const auto &resource: resources) {
            for (const auto &ngb : resource.position.neighbours()) {
                if (!Data::insideMap(ngb) || extendedDangerField.nonZeroUnsafe(ngb)) {
                    continue;
                }
                if (pathHotspot.getMarkUnsafe(ngb) > IGNORE_RESOURCE_DIGGING_IN_HOTSPOTS_KOEFF ||
                    workersDistanceField.getMarkUnsafe(ngb) == std::numeric_limits<int>::max()) {
//                    MapDrawing::drawSquare(ngb);
                    continue;
                }
                resourcePtrMarker.justPutMarkUnsafe(ngb); // currmark - доступные для раскопок ресурсы
            }
        }
        int nextValue = 1 + resourcePtrMarker.currentMark;
        for (auto &worker : me.workers) {
            if (resourcePtrMarker.checkMarkUnsafe(worker.position)) {
                ++Data::expectedResource;
                worker.action = std::make_unique<MyAction>(ActionType::DIG, worker.position);
                resourcePtrMarker.putValUnsafe(worker.position, nextValue); // currmark +1 - уже копаемое
            }
        }
        ++nextValue;
        for (const auto &resource: resources) {
            for (const auto &ngb : resource.position.neighbours()) {
                if (resourcePtrMarker.checkMark(ngb)) {
//                    MapDrawing::drawSquare(ngb);
                    resourcePtrMarker.putValUnsafe(ngb, nextValue); // некопаемое
                    foodDistanceField.putValUnsafe(ngb, 0);
                }
            }
        }

        resourcePtrMarker.currentMark = nextValue;

        Utils::forceMoveToPoints(resourcePtrMarker,
                                 me.workers,
                                 [](const MyEntity &entity) {
                                     return entity.action.operator bool();
                                 },
                                 [&foodDistanceField](const Vec2Int &pt, MyEntity &entity) {
                                     foodDistanceField.putValUnsafe(pt, std::numeric_limits<int>::max());
                                     entity.action = std::make_unique<MyAction>(ActionType::MOVE_TO, pt);
                                 });
//        MapDrawing::drawYesNoField(ignored, 0xFF0000, 8);
    }
}

#endif //AICUP2020_RESOURCECOLLECTION_H
