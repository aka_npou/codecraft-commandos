//
// Created by dragoon on 01.12.2020.
//

#ifndef AICUP2020_MOVEMENTS_H
#define AICUP2020_MOVEMENTS_H

namespace Movements {

    inline static Vec2Int moveTo(MyEntity &unit,
                                 const MyEntity &aimEntity,
                                 EntityAction &entityAction,
                                 int enoughDistance = 1) {
        Vec2Int vc = Utils::firstStepTo(unit, aimEntity, enoughDistance);
        if (vc.x != -1) {
            entityAction.moveAction = std::make_shared<MoveAction>(vc,
                                                                   false,
                                                                   false);

        } else {
#ifdef DEBUG_ENABLED
            std::cerr << "Path not found to ENTITY" << std::endl;
#endif
        }
        return vc;
    }

    inline static Vec2Int moveTo(MyEntity &unit,
                                 const Vec2Int &aimPosition,
                                 EntityAction &entityAction,
                                 int enoughDistance = 0) {
        Vec2Int vc = Utils::firstStepTo(unit, aimPosition, enoughDistance);

        if (vc.x != -1) {
            entityAction.moveAction = std::make_shared<MoveAction>(vc,
                                                                   false,
                                                                   false);
            unit.action = std::make_unique<MyAction>(MOVE_TO, aimPosition);
            return vc;
        } else {
#ifdef DEBUG_ENABLED
            std::cerr << "Path not found for move" << std::endl;
#endif
            entityAction.moveAction = std::make_shared<MoveAction>(aimPosition,
                                                                   true,
                                                                   false);
            return aimPosition;
        }
    }

    inline static void builderBuild(const MyEntity &unit, EntityAction &entityAction) {
        const MyAction &action = *unit.action;
        entityAction.buildAction = std::make_shared<BuildAction>(action.entityType, action.target);
    }

    inline static bool tryDig(MyEntity &entity, EntityAction &entityAction) {
        if (entity.action && !(entity.action->target == entity.position)) {
            auto resource = Data::get(entity.action->target);
            if (resource == nullptr) {
#ifdef DEBUG_ENABLED
                throw std::exception();
#endif
            }
            if (resource->actionsCount <= resource->health) {
                return false;
            } else {
                --resource->actionsCount;
            }
        }
        MyEntity *foundEntity = nullptr;
        for (const auto &ngb : entity.position.neighbours()) {
            auto currResource = Data::get(ngb);
            if (currResource == nullptr || currResource->entityType != RESOURCE) {
                continue;
            }
            if (foundEntity == nullptr) {
                foundEntity = currResource;
            } else {
                if (foundEntity->health <= foundEntity->actionsCount ||
                    (foundEntity->health - foundEntity->actionsCount >
                     currResource->health - currResource->actionsCount &&
                     currResource->health > currResource->actionsCount)) {
                    foundEntity = currResource;
                }
            }
        }
        bool switched = false;
        if (foundEntity != nullptr) {
            ++foundEntity->actionsCount;
            entityAction.attackAction = std::make_shared<AttackAction>(std::make_shared<int>(foundEntity->id), nullptr);
            if (entity.action->target != foundEntity->position) {
                switched = true;
                entity.action->target = foundEntity->position;
            }
        }
#ifdef DEBUG_ENABLED
        else {
            std::cout << "Resource not found!!!" << std::endl;
        }
#endif
        return switched;
    }

    inline static void builderRepair(MyEntity &unit, EntityAction &entityAction) {
        MyEntity *foundEntityToRepair = nullptr;
        // choose best thing to repair
        for (const auto &ngb: unit.position.neighbours()) {
            if (!Data::insideMap(ngb)) {
                continue;
            }
            auto entity = Data::getUnsafe(ngb);
            if (entity == nullptr || entity->health == entity->maxHealth() || entity->playerId != unit.playerId ||
                entity->id < 0) {
                continue;
            }
            if (entity->entityType != unit.action->entityType) {
                if (foundEntityToRepair == nullptr) {
                    foundEntityToRepair = entity;
                    continue;
                }
            }
            if (!entity->active) {
                entityAction.repairAction = std::make_shared<RepairAction>(entity->id);
                return;
            }
            foundEntityToRepair = entity;
        }
        if (foundEntityToRepair != nullptr) {
            entityAction.repairAction = std::make_shared<RepairAction>(foundEntityToRepair->id);
        } else {
            // есть чё копнуть?
            if (!tryDig(unit, entityAction)) {
                entityAction.repairAction = std::make_shared<RepairAction>(Data::maxEntityId + 3); // не ну а чё?
            }
        }
    }

    const int maxDeep = 4;
    int choosedDirections[maxDeep + 1];
    int maxInfluence;

    inline static void applyDirections(MyEntity &entity) {
//        MapDrawing::drawSquare(pos, 9);
        Vec2Int pos = entity.position;
        for (int time = 0; time <= maxDeep; ++time) {
            pos = pos + PathMap::direction(choosedDirections[time]);
//            MapDrawing::drawSquare(pos, 9);
            PathMap::putDirection(pos, time, choosedDirections[time]);
        }
        entity.action = std::make_unique<MyAction>(RETREAT, pos);
    }

    inline static int getRetreatSearchScore(const Vec2Int &pos) {
        const auto &enemyDistance = Mark::field[ENEMY_MIL_DISTANCE_FIELD];
        const auto &myDistance = Mark::field[MY_DISTANCE_FIELD];
        const auto &enemyDanger1 = Mark::field[ENEMY_DANGER_FIELD];
        const auto &enemyDangerExt = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        const auto &enemyDangerExt2 = Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2];
        return (enemyDistance.getMarkUnsafe(pos) * 2 -
                myDistance.getMarkUnsafe(pos) -
                enemyDanger1.getMarkUnsafe(pos) * 15 -
                enemyDangerExt.getMarkUnsafe(pos) * 10 -
                enemyDangerExt2.getMarkUnsafe(pos) * 5) * 2 + (Data::getUnsafe(pos) == nullptr);
    }

    inline static bool searchRetreatPath(int time, const Vec2Int &pos, int currInfluence) {
        bool foundBetter = false;
        for (const auto &i : Utils::shuffledOrder()) {
            Vec2Int nextStep = PathMap::direction(i) + pos;
            if (!Data::insideMap(nextStep) ||
                Mark::field[BLOCK_STATIC].checkMarkUnsafe(nextStep) ||
                !PathMap::couldGoHere(nextStep, time, pos)) {
                continue;
            }
            if (time == maxDeep) {
                int newMax = currInfluence + getRetreatSearchScore(nextStep);
                if (newMax > maxInfluence) {
                    maxInfluence = newMax;
                    foundBetter = true;
                    choosedDirections[time] = i;
                }
            } else {
                if (searchRetreatPath(time + 1, nextStep, currInfluence + getRetreatSearchScore(nextStep))) {
                    foundBetter = true;
                    choosedDirections[time] = i;
                }
            }
        }

        return foundBetter;
    }

    inline static void retreat(MyEntity &entity, EntityAction &entityAction) {
        if (entity.moved()) {
            Utils::removePath(entity);
        }
        maxInfluence = std::numeric_limits<int>::min();

        if (searchRetreatPath(0, entity.position, 0)) {
            applyDirections(entity);
            entity.nextPt = PathMap::direction(choosedDirections[0]) + entity.position;
//            entityAction.clean();
//            if (entity.nextPt == entity.position) {
//                doSmth(entity, entityAction);
//            }
            entityAction.moveAction = std::make_shared<MoveAction>(
                    entity.nextPt,
                    false,
                    false);
        } else {
            entity.nextPt = {-1, -1};
        }
    }

    inline bool moveAction(MyEntity &unit) {
        return unit.action->actionType == MOVE_TO || unit.action->actionType == RETREAT;
    }

    inline void applyNewAction(MyEntity &unit, MyAction &action, Vec2Int &nextStep, EntityAction &entityAction) {
        if (action.actionType == RETREAT) {
            return;
        }
        unit.action = std::make_unique<MyAction>(MOVE_TO, action.target);
        entityAction.clean();
        entityAction.moveAction = std::make_shared<MoveAction>(nextStep, false, false);
    }

    static bool tryMoveAndPush(MyEntity &unit,
                               MyAction &pushAction,
                               EntityAction &entityAction,
                               std::unordered_map<int, EntityAction> &entityActions) {
        if (pushAction.actionType == MOVE_TO && pushAction.target == unit.position) {
#ifdef DEBUG_ENABLED
            std::cout << "Can't be pushed to self point" << std::endl;
#endif
            return false;
        }
        Vec2Int pt;
        if (pushAction.actionType == RETREAT) {
            retreat(unit, entityAction);
            pt = unit.nextPt;
        } else {
            if (unit.moved()) {
                Utils::removePath(unit);
            }
            pt = Utils::firstStepTo(unit, pushAction.target, 0);
        }
        if (pt.x == -1) {
#ifdef DEBUG_ENABLED
            std::cout << "Unsuccessfull try to move" << std::endl;
#endif
            return false;
        }
        if (!Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2].checkMarkUnsafe(pt)) {
            retreat(unit, entityAction); // push entity away
            return false;
        } else {
            auto pushedEntity = Data::getUnsafe(pt);
            if (pushedEntity == nullptr) {
                applyNewAction(unit, pushAction, pt, entityAction);
#ifdef DEBUG_ENABLED
                std::cout << "Path found!" << std::endl;
#endif
                return true;
            }
            if (pushedEntity->id == unit.id) {
#ifdef DEBUG_ENABLED
                std::cerr << "Cant self push!" << std::endl;
#endif
                return false;
            }
            // it's planned to do smth... shame
            if (pushedEntity->entityType == BUILDER_UNIT &&
                (!pushedEntity->action || pushedEntity->action->actionType != MOVE_TO)) {
                if (tryMoveAndPush(*pushedEntity, pushAction, entityActions[pushedEntity->id], entityActions)) {
                    applyNewAction(unit, pushAction, pt, entityAction);
                    return true;
                }
            }
            Utils::removePath(unit);
            return false;
        }
#ifdef DEBUG_ENABLED
        std::cerr << "Unsuccessfull try to move" << std::endl;
#endif
        return true;
    }

    inline static void stayFor(MyEntity &entity, int time) {
        if (entity.moved()) {
#ifdef DEBUG_ENABLED
            std::cerr << "This entity should do smth!" << std::endl;
#endif
            Utils::removePath(entity);
        }
        entity.nextPt = entity.position;
        for (int i = 0; i != time; ++i) {
            int currDir = PathMap::directionFrom(entity.position, i);
            if (currDir < 0) {
                PathMap::putDirection(entity.position, i, 4);
            } else {
                break;
            }
        }
    }

    inline void removeIdFrom(int id, std::vector<int> &dest) {
        auto it = std::find(dest.begin(), dest.end(), id);
        if (it == dest.end()) {
            return;
        }
        *it = *dest.rbegin();
        dest.pop_back();
    }

    inline static void applyWorkerActions(std::vector<MyEntity> &entities,
                                          std::unordered_map<int, EntityAction> &entityActions) {
        auto &extendedDangerField = Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2];
        auto &slightlyExtendedDangerField = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        std::vector<int> retreatEntities;
        std::vector<int> moveEntities;
        std::vector<int> builderMoveEntities;
        std::vector<int> digEntities;
        std::vector<int> healing;
        std::vector<int> repairEntities;
        std::vector<int> buildEntities;
        std::vector<int> nothingToDoUnits;
        for (int i = 0; i != entities.size(); ++i) {
            auto &unit = entities[i];
            EntityAction &entityAction = (entityActions.insert(std::make_pair(unit.id, EntityAction())).first->second);
            // IF №1
            if (!slightlyExtendedDangerField.checkMarkUnsafe(unit.position)) {
                retreatEntities.push_back(i);
                continue;
            }
            for (const auto &ngb : unit.position.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto nearEntity = Data::getUnsafe(ngb);
                // впрочем врага тоже может и имеет смысл стукнуть... Но это потом
                if (nearEntity == nullptr || !nearEntity->canMove() || nearEntity->playerId != unit.playerId) {
                    continue;
                }
                if (nearEntity->entityType == BUILDER_UNIT || nearEntity->entityType == RANGED_UNIT) {
                    if (nearEntity->maxHealth() >= nearEntity->health + 5) {
                        healing.push_back(i);
                    }
                }
            }
            if (!extendedDangerField.checkMarkUnsafe(unit.position)) {
                // IF ниже связан с ифом №1
                if (!unit.action || unit.action->actionType != REPAIR || unit.action->target != unit.position) {
                    retreatEntities.push_back(i); // при ремонте отступаем чуть попожжа
                    continue;
                }
            }
            if (!unit.action) {
                nothingToDoUnits.push_back(i);
                continue;
            }
            const auto &action = *unit.action;
            switch (action.actionType) {
                case MOVE_TO:
                    moveEntities.push_back(i);
                    break;
                case ATTACK_TARGET:
                    break;
                case REPAIR:
                    if (unit.position != action.target) {
                        moveEntities.push_back(i);
                    } else {
                        repairEntities.push_back(i);
                    }
                    break;
                case DIG:
                    if (action.target.distance(unit.position) == 0) {
                        digEntities.push_back(i);
                    } else {
                        moveEntities.push_back(i);
                    }
                    break;
                case BUILD:
                    if (unit.distanceToEntity(unit.action->target) == 1) {
                        buildEntities.push_back(i);
                    } else {
                        builderMoveEntities.push_back(i);
                    }
                    break;
            }
            entityActions[unit.id] = entityAction;
        }
        for (auto &id : healing) {
            auto &unit = entities[id];
            for (const auto &ngb: unit.position.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto nearEntity = Data::getUnsafe(ngb);
                if (nearEntity == nullptr || !nearEntity->canMove() || nearEntity->playerId != unit.playerId) {
                    continue;
                }

                if (nearEntity->entityType == BUILDER_UNIT || nearEntity->entityType == RANGED_UNIT) {
                    int health = nearEntity->health + nearEntity->actionsCount;
                    if (nearEntity->maxHealth() >= health + 5) {
                        entityActions[unit.id].repairAction = std::make_shared<RepairAction>(nearEntity->id);
                        ++nearEntity->actionsCount;
                        removeIdFrom(id, moveEntities);
                        removeIdFrom(id, builderMoveEntities);
                        removeIdFrom(id, digEntities);
                        removeIdFrom(id, repairEntities);
                        removeIdFrom(id, buildEntities);
                        removeIdFrom(id, nothingToDoUnits);
                        stayFor(unit, 1);
                        break;
                    }
                }
            }
        }
        // RETREAT
        for (auto &id : retreatEntities) {
            auto &unit = entities[id];
            retreat(unit, entityActions[unit.id]);
        }
        // MOVE & RETREAT
        auto applyMove = [&extendedDangerField, &slightlyExtendedDangerField](MyEntity &unit, EntityAction &entityAction) {
            auto pt = moveTo(unit, unit.action->target, entityAction);
            if (!extendedDangerField.checkMarkUnsafe(pt)) {
                if (unit.action->target != pt ||
                    !slightlyExtendedDangerField.checkMarkUnsafe(pt) ||
                    !Mark::field[REPAIR_PLACES].checkMark(pt)) {
                    retreat(unit, entityAction);
                }
            }
        };

        for (auto &id : moveEntities) {
            auto &unit = entities[id];
            auto &ea = entityActions[unit.id];
            applyMove(unit, ea);
        }

        for (auto &id : builderMoveEntities) {
            auto &unit = entities[id];
            auto &ea = entityActions[unit.id];
            auto pt = moveTo(unit, *Data::getUnsafe(unit.action->target), ea);
            if (!extendedDangerField.checkMarkUnsafe(pt)) {
                retreat(unit, ea);
            }
        }

        for (auto &id : buildEntities) {
            auto &unit = entities[id];
            if (moveAction(unit)) {
                continue;
            }
            auto &ea = entityActions[unit.id];
            int dirFrom = PathMap::directionFrom(unit.position, 0);
            // освободить проход кому-то
            if (dirFrom >= 0) {
                Vec2Int from = unit.position + PathMap::reverseDirection(dirFrom);
                auto entity = Data::getUnsafe(from);
                if (!tryMoveAndPush(unit, *entity->action, ea, entityActions)) {
                    if (moveAction(unit)) {
                        continue;
                    }
                    builderBuild(unit, ea);
                }
            } else {
                builderBuild(unit, ea);
            }
        }

        for (auto &id: repairEntities) {
            auto &unit = entities[id];
            if (moveAction(unit)) {
                continue;
            }
            auto &ea = entityActions[unit.id];
            int dirFrom = PathMap::directionFrom(unit.position, 0);
            // освободить проход кому-то
            if (dirFrom >= 0) {
                Vec2Int from = unit.position + PathMap::reverseDirection(dirFrom);
                auto entity = Data::getUnsafe(from);
                if (!tryMoveAndPush(unit, *entity->action, ea, entityActions)) {
                    if (moveAction(unit)) {
                        continue;
                    }
                    builderRepair(unit, ea);
                }
            } else {
                builderRepair(unit, ea);
            }
        }

        bool switchedAny = true;
        int digProcessCount = 0;
        while (switchedAny) {
            ++digProcessCount;
            switchedAny = false;
            for (auto &id: digEntities) {
                auto &unit = entities[id];
                if (moveAction(unit)) {
                    continue;
                }
                auto &ea = entityActions[unit.id];
                int dirFrom = PathMap::directionFrom(unit.position, 0);
                // освободить проход кому-то
                if (dirFrom >= 0) {
                    Vec2Int from = unit.position + PathMap::reverseDirection(dirFrom);
                    auto entity = Data::getUnsafe(from);
#ifdef DEBUG_ENABLED
                    if (!entity->action) {
                        throw std::exception();
                    }
#endif
                    if (!tryMoveAndPush(unit, *entity->action, ea, entityActions)) {
                        if (moveAction(unit)) {
                            continue;
                        }
                        switchedAny |= tryDig(unit, ea);
                    }
                } else {
                    switchedAny |= tryDig(unit, ea);
                }
            }
        }
        for (auto &unit : entities) {
            if (!unit.action) {
                continue;
            }
            if (unit.action->actionType == DIG || unit.action->actionType == REPAIR) {
                stayFor(unit, 2);
            }
        }

        for (auto &id : nothingToDoUnits) {
            auto &unit = entities[id];
            auto pt = Utils::findClosestForCivilianMatch(unit.position, Mark::instances[0], Utils::isSquareUnseen);
            if (pt.x < 0) {
                retreat(unit, entityActions[unit.id]);
            } else {
                unit.action = std::make_unique<MyAction>(MOVE_TO, pt);
                applyMove(unit, entityActions[unit.id]);
            }
        }
#ifdef REWIND_VIEWER
        RewindClient::instance().message("Dig wswitch count %d", digProcessCount);
#endif
    }

    inline static void applyBasesActions(const std::vector<MyEntity> &entities,
                                         std::unordered_map<int, EntityAction> &entityActions) {
        for (const auto &unit : entities) {
            if (!unit.action) {
                EntityAction entityAction;
                entityActions[unit.id] = entityAction;
                continue;
            }
            const auto &action = *unit.action;
            EntityAction entityAction;
            switch (action.actionType) {
                case BUILD:
                    entityAction.buildAction = std::make_shared<BuildAction>(
                            action.entityType,
                            action.target);
                    break;
                default:
                    std::cerr << "Oops, smth wrong with action for unitId " << unit.id << std::endl;
                    break;
            }
            entityActions[unit.id] = entityAction;
        }
    }

}
#endif //AICUP2020_MOVEMENTS_H
