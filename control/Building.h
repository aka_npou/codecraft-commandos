//
// Created by dragoon on 02.12.2020.
//

#ifndef AICUP2020_BUILDING_H
#define AICUP2020_BUILDING_H

namespace Building {

    //Расстояния поиска если юнитов столько
    int buildTable[10][11] = {{0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0}, //WALL
                              {6,  6,  5,  4,  3,  3,  3, 2, 2, 2, 2}, //HOUSE
                              {10, 10, 10, 10, 10, 10, 9, 8, 7, 6, 5}, //BUILDER_BASE
                              {0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0}, //BUILDER_UNIT
                              {10, 10, 10, 10, 10, 10, 9, 8, 7, 6, 5}, //MELEE_BASE
                              {0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0}, //MELEE_UNIT
                              {10, 10, 10, 10, 10, 10, 9, 8, 7, 6, 5}, //RANGED_BASE
                              {0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0}, //RANGED_UNIT
                              {0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0}, //RESOURCE
                              {8,  8,  6,  4,  4,  3,  3, 2, 2, 2, 2}}; //TURRET

    void repairBuilding(PlayerData &data, const MyEntity &building) {
        auto &repairPoints = Mark::instances[1];
        auto &marker = Mark::instances[2];
        auto &staticBlockField = Mark::field[BLOCK_STATIC];

        int toRepair = (MyEntity::props[building.entityType].maxHealth - building.health);
        if (toRepair == 0) {
            return;
        }

        auto freeNeighbours = Utils::neighbours(building.position, building.entityType);
        Utils::eraseMatched<Vec2Int>(freeNeighbours, [&data](const Vec2Int &pos) {
            if (Mark::field[REPAIR_PLACES].checkMark(pos) ||
                Mark::field[ENEMY_DANGER_EXTENDED_FIELD].getMarkUnsafe(pos) > 0) { // не подходим на расстояние вытянутого меча
                return true;
            }
            auto entity = Data::get(pos);
            return entity != nullptr &&
                   (entity->entityType != BUILDER_UNIT || entity->playerId != data.id());
        });
        ++repairPoints.currentMark;
        for (const auto &pt : freeNeighbours) {
            repairPoints.putMarkUnsafe(pt);
            staticBlockField.putValUnsafe(pt, staticBlockField.currentMark - 1);
//                    MapDrawing::drawSquare(pt, 0xFF0000, 5);
        }
        ++marker.currentMark;
        Utils::doForAllInUnitRange(1, building.position, building.size(),
                                   [&marker](const Vec2Int &pt) {
                                       marker.putMarkUnsafe(pt);
                                   });

        int totalWorkers = 0;
        int step = 0;
        auto expanding = freeNeighbours;

        while (true) {
            for (const auto &pt : expanding) {
//                        MapDrawing::drawSquare(pt, 0xFF0000 | ((25 * step + 25) << 24), 9);
                auto entity = Data::getUnsafe(pt);
                totalWorkers += (entity != nullptr && entity->entityType == BUILDER_UNIT &&
                                 entity->playerId == data.id());
            }
            if (buildTable[building.entityType][step] <= totalWorkers) {
                break;
            }
            if (++step == 10) {
                break;
            }
            if ((totalWorkers + 1) * step >= toRepair) {
                --step;
                break;
            }
            Utils::expandBordersIgnoreMoveable(expanding, marker);
        }

        Utils::forceMoveToPoints(repairPoints,
                                 data.workers,
                                 [](const MyEntity &entity) {
                                     return entity.action && (entity.action->actionType == REPAIR);
                                     // || entity.action->actionType == BUILD
                                 },
                                 [buildingType = building.entityType](const Vec2Int &pos, MyEntity &entity) {
                                     if (entity.action && entity.action->actionType == BUILD) {
                                         return;
                                     }
                                     Mark::field[REPAIR_PLACES].putMarkUnsafe(pos);
                                     entity.action = std::make_unique<MyAction>(ActionType::REPAIR,
                                                                                pos,
                                                                                buildingType);
                                 }, std::min(buildTable[building.entityType][step], toRepair), step);
    }


    inline int apply_scores(int baseScore, const Vec2Int &pos, int playerId) {
        auto entity = Data::get(pos);
        if (entity == nullptr) {
            return 0;
        }
        if (entity->playerId && entity->playerId != playerId) {
            return -200 * baseScore;
        }
        if (entity->entityType != BUILDER_UNIT) {
            return 0;
        }
        if (!entity->action) {
            return 2 * baseScore;
        } else {
            if (entity->action->actionType == REPAIR) { // уже занят
                return 0;
            }
            return baseScore;
        }
    }

    MyEntity *tryRadius(const std::vector<Vec2Int> &neighbours) {
        int bestDist = 0;
        MyEntity *best = nullptr;
        for (const auto &position : neighbours) {
            auto entity = Data::get(position);
            if (entity == nullptr || entity->entityType != BUILDER_UNIT) {
                continue;
            }
            if (!entity->action) {
                return entity;
            }
            if (entity->action->actionType == BUILD || entity->action->actionType == REPAIR) {
                continue;
            }
            if (best == nullptr) {
                best = entity;
                bestDist = entity->action->target.distance(entity->position);
            } else {
                int newDist = entity->action->target.distance(entity->position);
                if (newDist > bestDist) {
                    best = entity;
                    bestDist = newDist;
                }
            }
        }
        return best;
    }

    inline static bool checkBuildingsNearby(const Vec2Int &pos, int houseSize) {
        for (int i = -1; i != houseSize; ++i) {
            auto entity = Data::get(pos.x + i, pos.y - 1); // x - -1 0 1 2
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
            entity = Data::get(pos.x + houseSize, pos.y + i); // y - -1 0 1 2
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
            entity = Data::get(pos.x - 1, pos.y + i + 1); // y - 0 1 2 3
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
            entity = Data::get(pos.x + i + 1, pos.y + houseSize); // x - 0 1 2 3
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
        }
        if (pos.x == 0 && pos.y == 0) {
            return false;
        }
        if (pos.x == 0 && pos.y <= houseSize) {
            for (int i = 0; i != pos.y; ++i) {
                auto entity = Data::getUnsafe(houseSize, i);
                if (entity == nullptr || entity->entityType == RESOURCE || entity->canMove()) {
                    return false;
                }
            }
            return true;
        }
        if (pos.x <= houseSize && pos.y == 0) {
            for (int i = 0; i != pos.x; ++i) {
                auto entity = Data::getUnsafe(i, houseSize);
                if (entity == nullptr || entity->entityType == RESOURCE || entity->canMove()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    constexpr int MIN_HOT_TO_RESTRICT = 5;
    constexpr int MIN_SUM_HOT_TO_RESTRICT = 8;

    inline bool checkBuildHousePlace(const Vec2Int &pos,
                                     int playerId,
                                     std::vector<std::pair<int, Vec2Int>> &places) {
        int houseSize = MyEntity::props[HOUSE].size;
        int dist = Mark::field[MY_WORKERS_DISTANCE_FIELD].getMarkUnsafe(pos + Vec2Int(houseSize / 2, houseSize / 2));
        if (dist > 10) {
            return false;
        }

        auto &hotspot = Mark::field[LAST_PATH_HOTSPOT];
        int sumHotspot = 0;
        int maxHotspot = 0;
        for (int x = 0; x != houseSize; ++x) {
            for (int y = 0; y != houseSize; ++y) {
                Vec2Int checkPos = pos + Vec2Int(x, y);
                if (Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2].getMarkUnsafe(checkPos) != 0 ||
                    Data::getUnsafe(checkPos) != nullptr) {
                    return false;
                }
                sumHotspot += hotspot.getMarkUnsafe(checkPos);
                maxHotspot = std::max(hotspot.getMarkUnsafe(checkPos), maxHotspot);
            }
        }
        if (sumHotspot >= MIN_SUM_HOT_TO_RESTRICT || maxHotspot >= MIN_HOT_TO_RESTRICT ||
            checkBuildingsNearby(pos, houseSize)) {
            return false;
        }
        int currScore = 0;
        auto neighbours = Utils::neighbours(pos, houseSize);
        auto &marker = Mark::instances[0];
        Utils::fillMark(pos, houseSize, marker);
        for (const auto &position : neighbours) {
            marker.putMark(position);
        }

        for (const auto &position : neighbours) {
            currScore += apply_scores(30, position, playerId);
            auto ent = Data::get(position);
            if (ent != nullptr && ent->entityType == RESOURCE) {
                currScore -= 10;
            }
        }
        Utils::expandBorders(neighbours, marker);
        for (const auto &position : neighbours) {
            currScore += apply_scores(20, position, playerId);
        }
        Utils::expandBorders(neighbours, marker);
        for (const auto &position : neighbours) {
            currScore += apply_scores(15, position, playerId);
        }
        if (currScore <= 0) {
            return false;
        }
        int value = Mark::field[ENEMY_MIL_DISTANCE_FIELD].mark[pos.x + 1][pos.y + 1];
        if (value != Mark::NO_ENEMY_DISTANCE) {
            currScore *= value * value;
        }
        if (pos.oneIsZero()) {
            currScore *= 3 / 2;
        }
        if (currScore > 0) {
            places.emplace_back(currScore, pos);
            return true;
        }
        return false;
    }

    inline bool checkBuildBarrackPlace(const Vec2Int &pos,
                                       int playerId,
                                       EntityType entityType,
                                       std::vector<std::pair<int, Vec2Int>> &places) {
        int milDistance = Mark::field[ENEMY_MIL_DISTANCE_FIELD].mark[pos.x + 1][pos.y + 1];
        if (milDistance < 30) {
            return false;
        }
        int buildingSize = MyEntity::props[entityType].size;
        int dist = Mark::field[MY_WORKERS_DISTANCE_FIELD].getMarkUnsafe(
                pos + Vec2Int(buildingSize / 2, buildingSize / 2));
        if (dist > 12) {
            return false;
        }

        for (int x = 0; x != buildingSize; ++x) {
            for (int y = 0; y != buildingSize; ++y) {
                Vec2Int checkPos = pos + Vec2Int(x, y);
                if (Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2].getMarkUnsafe(checkPos) != 0 ||
                    Data::getUnsafe(checkPos) != nullptr) {
                    return false;
                }
            }
        }
        if (checkBuildingsNearby(pos, buildingSize)) {
            return false;
        }
        // скоринг переделать
        int currScore = 0;
        auto neighbours = Utils::neighbours(pos, buildingSize);
        auto &marker = Mark::instances[0];
        Utils::fillMark(pos, buildingSize, marker);
        for (const auto &position : neighbours) {
            marker.putMark(position);
        }
        const int maxSearch = 10;
        int scores[maxSearch] = {50, 45, 40, 35, 30, 25, 20, 15, 10, 5};
        for (int i = 0; i != maxSearch; ++i) {
            int cnt = 0;
            for (const auto &position : neighbours) {
                int score = apply_scores(scores[i], position, playerId);
                if (score > 0) {
                    ++cnt;
                }
                currScore += score;
            }
            if (cnt > 10) {
                break;
            }
            if (i + 1 < maxSearch) {
                Utils::expandBordersIgnoreMoveable(neighbours, marker);
            }
        }
        if (currScore <= 0) {
            return false;
        }
        if (milDistance != Mark::NO_ENEMY_DISTANCE) {
            currScore *= milDistance * milDistance;
        }

        int min = 80;
        Utils::doForAllPts({39, 39}, 2, [&min, &pos, &buildingSize](const Vec2Int &pt) {
            min = std::min(min, Utils::distanceTo(pt, pos, buildingSize));
        });
        min = 67 - min; // максимальное расстояние - 66
        currScore *= min;

        if (currScore > 0) {
            places.emplace_back(currScore, pos);
            return true;
        }
        return false;
    }

    bool checkBuildTurretPlace(const Vec2Int &pos, int myId, std::vector<std::pair<int, Vec2Int>> &places) {
        int milDistance = Mark::field[ENEMY_MIL_DISTANCE_FIELD].mark[pos.x + 1][pos.y + 1];
        if (milDistance < 30) {
            return false;
        }
        int buildingSize = MyEntity::props[TURRET].size;
        int dist = Mark::field[MY_WORKERS_DISTANCE_FIELD].getMarkUnsafe(
                pos + Vec2Int(buildingSize / 2, buildingSize / 2));
        if (dist > 4) {
            return false;
        }

        bool busy = false;
        Utils::doForAllPts(pos, 2, [&busy](const Vec2Int &pos) {
            busy |= Data::getUnsafe(pos) != nullptr;
        });
        if (busy) {
            return false;
        }

        int resourceCnt = 0;
        Utils::doForAllInUnitRange(7, pos, buildingSize, [&resourceCnt](const Vec2Int &pos) {
            auto entity = Data::getUnsafe(pos);
            if (entity == nullptr || entity->entityType != RESOURCE) {
                return;
            }
            resourceCnt += entity->health;
        });
        if (resourceCnt < 700) {
            return false;
        }

        auto neighbours = Utils::neighbours(pos, buildingSize);
        auto &marker = Mark::instances[0];
        Utils::fillMark(pos, buildingSize, marker);
        for (const auto &position : neighbours) {
            marker.putMark(position);
        }

        int closest = 300;
        for (const auto &turret : Data::me.turrets) {
            Utils::doForAllPts(pos, 2, [&closest, &turret](const Vec2Int &vc) {
                Utils::doForAllPts(turret.position, turret.size(), [&vc, &closest](const Vec2Int &pos) {
                    closest = std::min(closest, pos.distance(vc));
                });
            });
            if (closest < 5) {
                return false;
            }
        }

        int workersScore = 0;
        const int maxSearch = 5;
        int scores[maxSearch] = {50, 45, 40, 35, 30};
        for (int i = 0; i != maxSearch; ++i) {
            int cnt = 0;
            for (const auto &position : neighbours) {
                int score = apply_scores(scores[i], position, myId);
                if (score > 0) {
                    ++cnt;
                }
                workersScore += score;
            }
            if (cnt > 8) {
                break;
            }
            if (i + 1 < maxSearch) {
                Utils::expandBordersIgnoreMoveable(neighbours, marker);
            }
        }

        places.emplace_back(workersScore * resourceCnt, pos);
        return true;
    }

    inline bool checkBuildPlace(const Vec2Int &pos,
                                int playerId,
                                EntityType entityType,
                                std::vector<std::pair<int, Vec2Int>> &places) {
        switch (entityType) {
            case HOUSE:
                return checkBuildHousePlace(pos, playerId, places);
            case TURRET:
                return checkBuildTurretPlace(pos, playerId, places);
            default:
                return checkBuildBarrackPlace(pos, playerId, entityType, places);
        }
    }

    inline int extraHousesToBuild(int basicCost, int resources, int houses) {
        const auto &houseProps = MyEntity::props[EntityType::HOUSE];
        int cnt = 0;
        int ticksCount = 0;
        while (resources >= basicCost && ticksCount < 20) {
            if (houses <= 0) {
                resources -= houseProps.cost;
                if (resources < 0) {
                    return cnt + (cnt > 1);
                }
                houses += houseProps.populationProvide;
                ++cnt;
            }
            ++ticksCount;
            --houses;
            resources -= basicCost;
            ++basicCost;
        }
        return cnt;
    }

    static int houses_needed(PlayerData &data) {
        const auto &houseProps = MyEntity::props[EntityType::HOUSE];
        if (data.player.resource < houseProps.cost) {
            return 0;
        }

        int inProgress = std::count_if(data.houses.begin(),
                                       data.houses.end(),
                                       [](const auto &val) { return !val.active; });
//    inProgress += std::count_if(data.meleeBase.begin(), data.meleeBase.end(), [](const auto &val) { return !val.active; });
//    inProgress += std::count_if(data.rangedBase.begin(), data.rangedBase.end(), [](const auto &val) { return !val.active; });
//    inProgress += std::count_if(data.builderBase.begin(), data.builderBase.end(), [](const auto &val) { return !val.active; });
#ifdef DEBUG_ENABLED
        std::cout << "In progress " << inProgress << std::endl;
#endif
        int cnt;
        {
            int baseHouses = data.totalHouses - data.totalUnits + inProgress * houseProps.populationProvide;
            cnt = extraHousesToBuild(MyEntity::props[EntityType::BUILDER_UNIT].cost + (int) data.workers.size(),
                                     data.player.resource,
                                     baseHouses);
            cnt = std::max(cnt,
                           extraHousesToBuild(
                                   MyEntity::props[EntityType::RANGED_UNIT].cost + (int) data.rangedUnits.size(),
                                   data.player.resource,
                                   baseHouses));
        }
        return cnt;
    }

    static bool blockPassage(Mark &refer, Mark &forUse, const Vec2Int &pos, int size) {
        for (int i = pos.x; i != pos.x + size; ++i) {
            for (int j = pos.y; j != pos.y + size; ++j) {
#ifdef DEBUG_ENABLED
                if (Data::get({i, j}) != nullptr) {
                    std::cerr << "Тут ничего не должно быть!" << std::endl;
                    Utils::thr();
                }
#endif
                Data::map[i][j] = Data::fakeObstacle();
            }
        }
        Utils::distanceFromBase(forUse);
        for (int i = pos.x; i != pos.x + size; ++i) {
            for (int j = pos.y; j != pos.y + size; ++j) {
                Data::map[i][j] = nullptr;
            }
        }
        int blockedAllowed = size * size + 2;
        int distDiffAllowed = 6000000;
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                auto diff = forUse.mark[i][j] - refer.mark[i][j];
                if (diff > 1000) {
                    --blockedAllowed;
                    if (blockedAllowed < 0) {
#ifdef DEBUG_ENABLED
                        std::cout << "Blocked: (" << pos.x << ',' << pos.y << ") of size " << size
                                  << " - too many fully blocked tiles" << std::endl;
#endif
                        return true;
                    }
                } else {
                    distDiffAllowed -= diff;
                    if (distDiffAllowed <= 0) {
#ifdef DEBUG_ENABLED
                        std::cout << "Blocked: (" << pos.x << ',' << pos.y << ") of size " << size
                                  << " - distance growth too bad for check at [" << i << ',' << j << "]" << std::endl;
#endif

                        return true;
                    }
                }
            }
        }
        return false;
    }

    static void build_smth(PlayerData &data, int needed, EntityType entityType) {
        const auto &entityProps = MyEntity::props[entityType];
        std::vector<std::pair<int, Vec2Int> > places;
        int maxPos = Data::mapSize - entityProps.size;
        for (int i = 0; i != maxPos; ++i) {
            for (int j = 0; j != maxPos; ++j) {
                checkBuildPlace({i, j}, data.id(), entityType, places);
            }
        }
        if (places.empty()) {
            return;
        }
//    for (auto &place : places) {
//        int dist = std::min(place.second.distance({0, 0}), 25);
//        place.first *= 1. + (25 - dist) / 5.;
//    }
        const auto placesComparator = [](const auto &first, const auto &second) {
            return first.first > second.first;
        };
        std::sort(places.begin(), places.end(), placesComparator);
        int i = -1;
        auto &baseDistance = Mark::instances[5];
        Utils::distanceFromBase(baseDistance);
        auto &newBaseDistance = Mark::instances[6];
        while (needed && data.player.resource >= entityProps.cost && ++i < (int) places.size()) {
            if (i > 0) {
                if (!checkBuildPlace(places[i].second, data.id(), entityType, places)) {
                    continue;
                }
                places[i] = *places.rbegin();
                places.pop_back();
                // ищем новое место под постройку
                if (i + 1 < (int) places.size() && places[i + 1].first > places[i].first) {
                    std::sort(places.begin() + i, places.end(), placesComparator);
                    --i;
                    continue;
                }
            }
            // тут проверить, что не закрываем проход
            if (blockPassage(baseDistance, newBaseDistance, places[i].second, entityProps.size)) {
                continue;
            }

            // а тут... уберём всю эту чушь и поставим просто первого попавшегося, видимо
            auto neighbours = Utils::neighbours(places[i].second, entityType);
            auto best = tryRadius(neighbours);
            if (best == nullptr) {
                continue;
            }
            if (best->action &&
                best->action->actionType == DIG &&
                best->action->target == best->position) {

                if (data.player.resource - 1 < entityProps.cost) {
#ifdef DEBUG_ENABLED
                    std::cout << "FIRED!" << std::endl;
#endif
                    return; // хватит, думаю
                }
            }
            --needed;
            data.player.resource -= entityProps.cost;
            best->action = std::make_unique<MyAction>(ActionType::BUILD, places[i].second, entityType);
            //Entity(int id, std::shared_ptr<int> playerId, EntityType entityType, Vec2Int position, int health, bool active);
            data.newBuildings.emplace_back(Entity(-354,
                                                  std::make_shared<int>(data.id()),
                                                  entityType,
                                                  places[i].second,
                                                  0,
                                                  false));

            Data::addAllToMap(data.newBuildings);// update all each time - reallocation possible

            Utils::doForAllPts(places[i].second, entityProps.size, [](const Vec2Int &pt) {
                Mark::field[BLOCK_STATIC].putMarkUnsafe(pt);
            });
            repairBuilding(data, *data.newBuildings.rbegin()); // сразу отправляем рабочих
        }
    }

    void repair(PlayerData &data) {
        Mark::field[REPAIR_PLACES].currentMark++;
        std::vector<std::vector<MyEntity> *> vcs;
        vcs.push_back(&data.builderBase);
        vcs.push_back(&data.rangedBase);
        vcs.push_back(&data.meleeBase);
        vcs.push_back(&data.turrets);
        vcs.push_back(&data.houses);
        vcs.push_back(&data.walls);
        for (const auto &buildings : vcs) {
            for (const auto &building : *buildings) {
                repairBuilding(data, building);
            }
        }
    }

}
#endif //AICUP2020_BUILDING_H


