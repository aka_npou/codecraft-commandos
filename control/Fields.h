//
// Created by dragoon on 06.12.2020.
//

#ifndef AICUP2020_FIELDS_H
#define AICUP2020_FIELDS_H

namespace Fields {

    static std::vector<Vec2Int> achievable(const MyEntity &entity, int steps) {
        if (steps == 0) {
            std::vector<Vec2Int> vc(1);
            vc[0] = entity.position;
            return vc;
        }
        auto &queueMarker = Mark::instances[1];
        ++queueMarker.currentMark;
        std::queue<std::pair<Vec2Int, int> > queue;
        queue.emplace(entity.position, 0);
        queueMarker.putMarkUnsafe(entity.position);
        std::vector<Vec2Int> result;
        result.push_back(entity.position);
        while (!queue.empty()) {
            auto curr = queue.front();
            queue.pop();
            int nextStep = curr.second + 1;
            for (const auto &ngb : curr.first.neighbours()) {
                if (queueMarker.putMark(ngb)) {
                    auto foundEntity = Data::getUnsafe(ngb);
                    if (Utils::noneOrMoveableAlly(foundEntity, entity.playerId)) {
                        result.push_back(ngb);
                        if (nextStep < steps) {
                            queue.emplace(ngb, nextStep);
                        }
                    }
                }
            }
        }
        return result;
    }

    static void applyDangerRanged(const std::vector<MyEntity> &units, Mark &field, int steps) {
        if (units.empty()) {
            return;
        }
        auto &secondQueueMarker = Mark::instances[4];

        int pointsToAdd;
        int size = units[0].size();
        int attackRange = units[0].attackRange();

        const auto putMarker = [&field, &secondQueueMarker, &pointsToAdd](const Vec2Int &pos) {
            if (secondQueueMarker.putMarkUnsafe(pos)) {
                field.addUnsafe(pos, pointsToAdd);
            }
        };
        for (const auto &ranged : units) {
            if (!ranged.active) {
                continue;
            }
            pointsToAdd = 1;
            if (ranged.entityType == TURRET) {
                int meleeCnt(0), rangedCnt(0);
                auto calc = [&meleeCnt, &rangedCnt, enemyId = ranged.playerId](const Vec2Int &pos) {
                    auto entity = Data::getUnsafe(pos);
                    if (entity == nullptr || entity->playerId == enemyId) {
                        return;
                    }
                    if (entity->entityType == RANGED_UNIT) {
                        ++rangedCnt;
                    } else if (entity->entityType == MELEE_UNIT) {
                        ++meleeCnt;
                    }
                };
                int meleeNearby, meleeInside, rangedNearby, rangedInside;
                Utils::doForAllInUnitRange(5, ranged.position, 2, calc);
                pointsToAdd = 6;
                meleeInside = meleeCnt;
                rangedInside = rangedCnt;
                Utils::doForAllInUnitRange(6, ranged.position, 2, calc);
                meleeNearby = meleeCnt - meleeInside * 2;
                rangedNearby = rangedCnt - rangedInside * 2;
                pointsToAdd -= (meleeCnt * 2 + rangedCnt + rangedNearby * 2 + meleeNearby * 4) / 3;
                if (pointsToAdd <= 0) {
                    continue;
                }
            }
            ++secondQueueMarker.currentMark;
            for (const auto startingPos: achievable(ranged, steps)) {
                Utils::doForAllInUnitRange(attackRange, startingPos, size, putMarker);
            }
        }
    }

    static void applyDangerFields(const PlayerData &other, Mark &direct, Mark &moved, Mark &moved2) {
        applyDangerRanged(other.meleeUnits, direct, 0);
        applyDangerRanged(other.rangedUnits, direct, 0);
        applyDangerRanged(other.turrets, direct, 0);

        applyDangerRanged(other.meleeUnits, moved, 1);
        applyDangerRanged(other.rangedUnits, moved, 1);
        applyDangerRanged(other.turrets, moved, 0);

        applyDangerRanged(other.meleeUnits, moved2, 2);
        applyDangerRanged(other.rangedUnits, moved2, 2);
        applyDangerRanged(other.turrets, moved2, 0);
    }

    static void applyDangerFields(std::vector<PlayerData> &others) {
        auto &enemyDirect = Mark::field[ENEMY_DANGER_FIELD];
        auto &enemyMoved = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        auto &movedField2 = Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2];
        for (const auto &other : others) {
            applyDangerFields(other, enemyDirect, enemyMoved, movedField2);
        }
//        MapDrawing::drawOverlapField(enemyDirect, 0xFF0000, 5);
//        MapDrawing::drawOverlapField(enemyMoved, 0xFF0000, 5);
//        MapDrawing::drawOverlapField(movedField2, 0xFF0000, 6);
    }

    static void calcMilDistanceField(const PlayerData &playerData, Mark &mark) {
        struct Queue {
            int priority;
            Vec2Int pos;

            Queue(int priority, const Vec2Int &pos) : priority(priority), pos(pos) {}

            bool operator<(const Queue &other) const {
                return this->priority > other.priority;
            }
        };
        std::priority_queue<Queue> queue;
        for (const auto &unit : playerData.rangedUnits) {
            queue.emplace(0, unit.position);
            mark.putValUnsafe(unit.position, 0);
        }
        for (const auto &unit : playerData.meleeUnits) {
            queue.emplace(0, unit.position);
            mark.putValUnsafe(unit.position, 0);
        }
        for (const auto &unit : playerData.turrets) {
            queue.emplace(0, unit.position);
            queue.emplace(0, unit.position + Vec2Int(1, 0));
            queue.emplace(0, unit.position + Vec2Int(0, 1));
            queue.emplace(0, unit.position + Vec2Int(1, 1));
            mark.currentMark = -1;
            Utils::fillMark(unit.position, 2, mark);
        }
        while (!queue.empty()) {
            auto curr = queue.top();
            queue.pop();
            for (const auto &ngb : curr.pos.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto ptr = Data::getUnsafe(ngb);
                int currStep;
                if (ptr == nullptr || ptr->canMove()) {
                    currStep = curr.priority + 1;
                } else if (ptr->entityType == RESOURCE) {
                    currStep = curr.priority + (ptr->health + 4) / 5;
                } else {
                    continue;
                }
                if (mark.newMinimumUnsafe(ngb, currStep)) {
                    queue.emplace(currStep, ngb);
                }
            }
        }
    }

    static void calcCivDistanceField(PlayerData &playerData, Mark &mark) {
        struct Queue {
            int priority;
            Vec2Int pos;

            Queue(int priority, const Vec2Int &pos) : priority(priority), pos(pos) {}

            bool operator<(const Queue &other) const {
                return this->priority > other.priority;
            }
        };
        std::priority_queue<Queue> queue;
        playerData.applyForAll([&mark, &queue](const std::vector<MyEntity> &entities) {
            for (const auto &entity : entities) {
                Utils::doForAllPts(entity.position, entity.size(), [&queue, &mark](const Vec2Int &pos) {
                    queue.emplace(0, pos);
                    mark.putValUnsafe(pos, 0);
                });
            }
        });
        while (!queue.empty()) {
            auto curr = queue.top();
            queue.pop();
            for (const auto &ngb : curr.pos.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto ptr = Data::getUnsafe(ngb);
                int currStep;
                if (ptr == nullptr || ptr->canMove()) {
                    currStep = curr.priority + 1;
                } else if (ptr->entityType == RESOURCE) {
                    currStep = curr.priority + (ptr->health + 4) / 5;
                } else {
                    continue;
                }
                if (mark.newMinimumUnsafe(ngb, currStep)) {
                    queue.emplace(currStep, ngb);
                }
            }
        }
    }

    static void calcWorkersDistanceField(const PlayerData &playerData, Mark &mark) {
        std::queue<std::pair<int, Vec2Int>> queue;
        for (const auto &unit : playerData.workers) {
            queue.emplace(0, unit.position);
            mark.putValUnsafe(unit.position, 0);
        }
        for (const auto &builderBase: playerData.builderBase) {
            Utils::doForAllPts(builderBase.position, builderBase.size(), [&queue, &mark](const Vec2Int &pos) {
                queue.emplace(0, pos);
                mark.putValUnsafe(pos, 0);
            });
        }
        while (!queue.empty()) {
            auto curr = queue.front();
            queue.pop();
            int nextStep = curr.first + 1;
            for (const auto &ngb : curr.second.neighbours()) {
                if (!Data::insideMap(ngb)) {
                    continue;
                }
                auto entity = Data::getUnsafe(ngb);
                if (entity != nullptr && !entity->canMove()) {
                    continue;
                }
                if (mark.newMinimumUnsafe(ngb, nextStep)) {
                    queue.emplace(nextStep, ngb);
                }
            }
        }
    }

    static void calcFogOfWar(PlayerData &playerData, Mark &mark) {
        ++mark.currentMark;
        auto &ngbsMarker = Mark::instances[0];
        playerData.applyForAll([&mark, &ngbsMarker](const std::vector<MyEntity> &myEntities) {
            if (myEntities.empty()) {
                return;
            }
            const auto &props = MyEntity::props[myEntities[0].entityType];
            int range = props.sightRange;
            for (const auto &entity : myEntities) {
                --mark.currentMark;
                Utils::fillMark(entity.position, props.size, mark);
                Utils::fillMark(entity.position, props.size, ngbsMarker);
                auto ngbs = Utils::neighbours(entity.position, props.size);
                Utils::filterBorders(ngbs);
                for (const auto &pos: ngbs) {
                    mark.justPutMarkUnsafe(pos);
                    ngbsMarker.justPutMarkUnsafe(pos);
                }
                for (int i = 1; i != range; ++i) {
                    Utils::expandIgnoreObstacles(ngbs, ngbsMarker);
                    for (const auto &pos: ngbs) {
                        mark.justPutMarkUnsafe(pos);
                    }
                }
            }
        });
        auto &lastSawField = Mark::field[LAST_SAW];
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (mark.checkMarkUnsafe({i, j})) {
                    lastSawField.putMarkUnsafe({i, j});
                }
            }
        }
    }

    static void updateTimeToGoFields() {
        //Data::mapSize
        auto &civTime = Mark::field[CIV_TIME_TO_GO];
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                auto entity = Data::getUnsafe({i, j});
                if (entity != nullptr && !entity->canMove()) {
                    civTime.putValUnsafe({i, j}, 0);
                } else {
                    civTime.putValUnsafe({i, j}, 1);
                }
            }
        }
        auto &milTime = Mark::field[MIL_TIME_TO_GO];
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                auto entity = Data::getUnsafe({i, j});
                if (entity == nullptr || entity->canMove()) {
                    milTime.putValUnsafe({i, j}, 1);
                } else if (entity->entityType == RESOURCE) {
                    milTime.putValUnsafe({i, j}, (entity->health + 4) / 5 + 1);
                } else {
                    continue;
                }
            }
        }
    }
}

#endif //AICUP2020_FIELDS_H
