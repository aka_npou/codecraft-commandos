//
// Created by dragoon on 13.12.2020.
//

#ifndef AICUP2020_UNITPRODUCING_H
#define AICUP2020_UNITPRODUCING_H

namespace UnitProducing {

    struct UnitsNeeded {
        int workers;
        int ranged;
        int melee;

        UnitsNeeded(int workers, int ranged, int melee) : workers(workers), ranged(ranged), melee(melee) {}
    };

    constexpr int maxWorkers = 60;

    static int workersNeeded(PlayerData &data, const std::vector<MyEntity> &resources) {
        if (data.workers.size() > maxWorkers) {
            return 0;
        }

        bool freeResourceDigPoints = false;
        {
            auto &resourcePtrMarker = Mark::field[RESOURCE_DIG_MARK];
            for (int i = 0; i != Data::mapSize; ++i) {
                for (int j = 0; j != Data::mapSize; ++j) {
                    if (resourcePtrMarker.checkMarkUnsafe({i, j})) {
                        freeResourceDigPoints = true;
                        break;
                    }
                }
                if (freeResourceDigPoints) {
                    break;
                }
            }
        }
        if (!freeResourceDigPoints) {
            return 0;
        }

        int totalResourcesPerUnit = 0;
        for (const auto &resource : resources) {
            totalResourcesPerUnit += resource.health;
        }
        totalResourcesPerUnit /= (data.workers.size() + 1);
        int currentBuilderCost = MyEntity::props[BUILDER_UNIT].cost + data.workers.size();
        if (currentBuilderCost * 3 < totalResourcesPerUnit) {
            return 1;
        }
        return 0;
    }

    inline static int unitsClose(std::vector<MyEntity> &entities, int distance) {
        return std::count_if(entities.begin(), entities.end(), [&distance](const MyEntity &entity) {
            return std::max(entity.position.x, entity.position.y) < distance;
        });
    }

    static Vec2Int bestBuildPos(const MyEntity &building, Mark &marker) {
        Vec2Int bestBuildPos = {-1, -1};
        int bestScore = std::numeric_limits<int>::max();
        for (const auto &buildPos: Utils::neighbours(building.position, building.entityType)) {
            if (Data::get(buildPos) != nullptr) {
                continue;
            }
            int currScore = marker.getMarkUnsafe(buildPos);
            if (bestScore > currScore) {
                bestScore = currScore;
                bestBuildPos = buildPos;
            }
        }
        return bestBuildPos;
    }

    static void buildWorker(PlayerData &data) {
        if (data.builderBase.empty() ||
            (data.player.resource < (MyEntity::props[BUILDER_UNIT].cost + static_cast<int>(data.workers.size())))) {
            return;
        }
        auto &marker = Mark::field[FOOD_DISTANCE];
        Utils::spreadDistance(marker);
//        MapDrawing::drawGradientField(marker);
        auto &builderBase = *data.builderBase.begin();
        Vec2Int buildPos = bestBuildPos(builderBase, marker);
        if (buildPos.x != -1) {
            builderBase.action = std::make_unique<MyAction>(ActionType::BUILD, buildPos, EntityType::BUILDER_UNIT);
            ++data.totalUnits;
            data.player.resource -= MyEntity::props[BUILDER_UNIT].cost + data.workers.size();
        }
    }

    static Vec2Int best_military(Vec2Int &position, int size) {
        int bestScore = -1;
        Vec2Int bestPos;
        auto &enemyDistance = Mark::field[ENEMY_MIL_DISTANCE_FIELD];
        auto &enemyCivDistance = Mark::field[ENEMY_CIVILIAN_DISTANCE_FIELD];
        auto &enemyDanger = Mark::field[ENEMY_DANGER_FIELD];
        auto &enemyDangerExt = Mark::field[ENEMY_DANGER_EXTENDED_FIELD];
        auto &enemyDangerExt2 = Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2];

        for (const auto &pos : Utils::neighbours(position, size)) {
            if (Data::get(pos) != nullptr) {
                continue;
            }
            int score = 1e9;
            if (enemyDangerExt.getMarkUnsafe(pos) == 1) {
                if (enemyDanger.getMarkUnsafe(pos) == 1) {
                    score += (int) 1e7;
                }
                score += (int) 1.1e5;
            }
            score -= enemyDistance.getMarkUnsafe(pos);
            score -= enemyDanger.getMarkUnsafe(pos) * (int) 1e6;
            score -= enemyDangerExt.getMarkUnsafe(pos) * (int) 1e5;
            score -= enemyDangerExt2.getMarkUnsafe(pos) * (int) 1e3;
            score -= enemyCivDistance.getMarkUnsafe(pos);
            if (score > bestScore) {
                bestScore = score;
                bestPos = pos;
            }
        }
        return bestScore >= 0 ? bestPos : Vec2Int(-1, -1);
    }

    static void buildRanged(PlayerData &data) {
        if (data.rangedBase.empty() ||
            (data.player.resource < MyEntity::props[RANGED_UNIT].cost + static_cast<int>(data.rangedUnits.size()))) {
            return;
        }

        auto &rangedBase = *data.rangedBase.begin();
        Vec2Int buildPos = best_military(rangedBase.position, MyEntity::props[RANGED_BASE].size);
        if (buildPos.x != -1) {
            rangedBase.action = std::make_unique<MyAction>(ActionType::BUILD, buildPos, EntityType::RANGED_UNIT);
            ++data.totalUnits;

            data.player.resource -= MyEntity::props[RANGED_UNIT].cost + data.rangedUnits.size();
        }
    }

    static void buildMelee(PlayerData &data) {
        if (data.meleeBase.empty() ||
            (data.player.resource < MyEntity::props[MELEE_UNIT].cost + static_cast<int>(data.meleeUnits.size()))) {
            return;
        }

        auto &meleeBase = *data.meleeBase.begin();
        Vec2Int buildPos = best_military(meleeBase.position, MyEntity::props[MELEE_BASE].size);
        if (buildPos.x != -1) {
            meleeBase.action = std::make_unique<MyAction>(ActionType::BUILD, buildPos, EntityType::MELEE_UNIT);
            ++data.totalUnits;

            data.player.resource -= MyEntity::props[MELEE_UNIT].cost + data.meleeUnits.size();
        }
    }

    static UnitsNeeded get_units_needed(PlayerData &data,
                                        std::vector<PlayerData> &others,
                                        std::vector<MyEntity> &resources,
                                        const PlayerView &playerView) {
        if (data.totalHouses <= data.totalUnits) {
            return {0, 0, 0};
        }
        int workersToBuild = workersNeeded(data, resources);
        int allUnits = 0;
        if (playerView.currentTick < 70) {
            for (auto &other : others) {
                allUnits = std::max(allUnits, unitsClose(other.rangedUnits, 50) + unitsClose(other.meleeUnits, 50));
            }
        } else {
            for (auto &other : others) {
                allUnits += unitsClose(other.rangedUnits, 50) + unitsClose(other.meleeUnits, 50);
            }
        }

        int myUnits = unitsClose(data.rangedUnits, 30) + unitsClose(data.meleeUnits, 30);
        int neededRanged, neededMelee(0);
        if (myUnits > allUnits) {
            neededRanged = 0;
        } else {
            neededRanged = allUnits - myUnits;
        }

//        if (playerView.fogOfWar) {
//            neededRanged = neededRanged * 3 / 2;
//        }

        if (data.rangedBase.empty()) {
            neededMelee = neededRanged;
        }

        if (workersToBuild == 0) {
            neededRanged += 10;
            if (data.totalHouses > data.totalUnits + 15) {
                neededMelee += 5;
            }
        }
        if (neededRanged && data.rangedUnits.size() > 40) {
            neededRanged = std::max(allUnits - myUnits, 0);
        }
        if (neededMelee && data.meleeUnits.size() > 20) {
            neededMelee = 0;
        }

        return {workersToBuild, neededRanged, neededMelee};
    }

    static void buildUnits(PlayerData &data, const UnitsNeeded &unitsNeeded) {
        if (unitsNeeded.ranged) {
            buildRanged(data);
        }
        if (unitsNeeded.melee) {
            buildMelee(data);
        }
        int resourcesReserved = Utils::resourcesForBuilding(RANGED_UNIT, data.rangedUnits.size(), unitsNeeded.ranged) +
                                Utils::resourcesForBuilding(MELEE_UNIT, data.meleeUnits.size(), unitsNeeded.melee);
        data.player.resource -= resourcesReserved;
        if (unitsNeeded.workers > 0) {
            buildWorker(data);
        }
        data.player.resource += resourcesReserved;
    }
}

#endif //AICUP2020_UNITPRODUCING_H
