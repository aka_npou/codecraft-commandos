#ifndef _MY_STRATEGY_HPP_
#define _MY_STRATEGY_HPP_


#ifdef REWIND_VIEWER
#include "rewind/RewindClient.h"
#include "rewind/MapDrawing.h"
#endif


#include "DebugInterface.hpp"
#include "model/Model.hpp"
#include "myModel/MyEntity.h"
#include "myModel/PlayerData.h"

class MyStrategy {

public:
    int newDamage, newKills;
    MyStrategy();
    Action getAction(const PlayerView& playerView, DebugInterface* debugInterface);
    void debugUpdate(const PlayerView& playerView, DebugInterface& debugInterface);
};

#endif