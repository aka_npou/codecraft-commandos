#include "DebugInterface.hpp"
#include "MyStrategy.hpp"
#include "TcpStream.hpp"
#include "model/Model.hpp"
#include "myModel/Utils.h"
#include <memory>
#include <string>

#ifdef DEBUG_ENABLED

#include <iostream>

#endif

#ifdef DEBUG_ENABLED

struct StepInfoMemory {
    Mark hotPathMark;
    Mark lastSaw;
    std::vector<MyEntity> resources;
    PlayerData me;
    std::vector<PlayerData> others;

    StepInfoMemory(const Mark &hotPathMark,
                   const Mark &lastSaw) : hotPathMark(hotPathMark), lastSaw(lastSaw) {
        this->resources = Data::resources;
        this->me = Data::me;
        this->others = Data::others;
    }

    void restore() {
        Mark::field[PATH_HOTSPOT] = hotPathMark;
        Mark::field[LAST_SAW] = lastSaw;
        Data::resources = this->resources;
        Data::me = this->me;
        Data::others = this->others;
    }
};

#endif

class Runner {
public:
    Runner(const std::string &host, int port, const std::string &token) {
        std::shared_ptr<TcpStream> tcpStream(new TcpStream(host, port));
        inputStream = getInputStream(tcpStream);
        outputStream = getOutputStream(tcpStream);
        outputStream->write(token);
        outputStream->flush();
    }

    void run() {
        DebugInterface debugInterface(inputStream, outputStream);
        MyStrategy myStrategy;
#ifdef DEBUG_ENABLED
        std::vector<ServerMessage::GetAction> messages;
        std::vector<StepInfoMemory> memory;
        messages.reserve(1000);
        memory.reserve(1000);
        try {
#endif
            while (true) {
                auto message = ServerMessage::readFrom(*inputStream);
                if (auto getActionMessage = std::dynamic_pointer_cast<ServerMessage::GetAction>(message)) {
#ifdef DEBUG_ENABLED
                    messages.push_back(*getActionMessage);
                    memory.emplace_back(Mark::field[PATH_HOTSPOT], Mark::field[LAST_SAW]);
#endif
                    ClientMessage::ActionMessage(myStrategy.getAction(getActionMessage->playerView,
                                                                      getActionMessage->debugAvailable ? &debugInterface
                                                                                                       : nullptr)).writeTo(
                            *outputStream);
                    outputStream->flush();
#ifdef DEBUG_ENABLED
                    if (getActionMessage->playerView.maxTickCount == getActionMessage->playerView.currentTick + 1) {
                        break;
                    }
#endif
                } else if (auto finishMessage = std::dynamic_pointer_cast<ServerMessage::Finish>(message)) {
                    break;
                } else if (auto debugUpdateMessage = std::dynamic_pointer_cast<ServerMessage::DebugUpdate>(message)) {
                    myStrategy.debugUpdate(debugUpdateMessage->playerView, debugInterface);
                    ClientMessage::DebugUpdateDone().writeTo(*outputStream);
                    outputStream->flush();
                }
            }
#ifdef DEBUG_ENABLED
        } catch (...) {
#ifdef REWIND_VIEWER
            RewindClient::instance().end_frame();
#endif
        }
        int tickNo;
        while (true) {
            std::cout << "Waiting for tick no to debug" << std::endl;
            std::cin >> tickNo;
#ifdef DEBUG_PATH
            std::cin >> Utils::pathToDraw;
#endif
            if (tickNo == -1) {
                break;
            }
            if (tickNo < 0 || tickNo >= (int) messages.size()) {
                std::cout << "out of bound" << std::endl;
                continue;
            }
            auto message = messages[tickNo];
            memory[tickNo].restore();
            try {
                myStrategy.getAction(message.playerView, message.debugAvailable ? &debugInterface : nullptr);
            } catch (...) {
#ifdef REWIND_VIEWER
                RewindClient::instance().end_frame();
#endif
            }
        }
#endif
    }

private:
    std::shared_ptr<InputStream> inputStream;
    std::shared_ptr<OutputStream> outputStream;
};

int main(int argc, char *argv[]) {
    std::string host = argc < 2 ? "127.0.0.1" : argv[1];
    int port = argc < 3 ? 31001 : atoi(argv[2]);
    std::string token = argc < 4 ? "0000000000000000" : argv[3];
    Runner(host, port, token).run();
    return 0;
}