#include <algorithm>
#include <random>
#include "Vec2Int.hpp"

std::random_device rd;
std::mt19937 g(rd());

Vec2Int::Vec2Int() {}

Vec2Int::Vec2Int(int x, int y) : x(x), y(y) {}

Vec2Int Vec2Int::readFrom(InputStream &stream) {
    Vec2Int result;
    result.x = stream.readInt();
    result.y = stream.readInt();
    return result;
}

void Vec2Int::writeTo(OutputStream &stream) const {
    stream.write(x);
    stream.write(y);
}

bool Vec2Int::operator==(const Vec2Int &other) const {
    return x == other.x && y == other.y;
}

bool Vec2Int::operator!=(const Vec2Int &other) const {
    return x != other.x || y != other.y;
}

std::array<Vec2Int, 4> Vec2Int::neighbours() const {
    return {Vec2Int(this->x + 1, this->y),
            Vec2Int(this->x, this->y + 1),
            Vec2Int(this->x - 1, this->y),
            Vec2Int(this->x, this->y - 1)};
}

std::array<Vec2Int, 4> Vec2Int::neighboursShuffled() const {
    std::array<Vec2Int, 4> a = neighbours();
    std::shuffle(a.begin(), a.end(), g);
    return a;
}

bool Vec2Int::match(int x, int y) const {
    return this->x == x && this->y == y;
}

int Vec2Int::distance(const Vec2Int &other) const {
    return std::abs(this->x - other.x) + std::abs(this->y - other.y);
}

bool Vec2Int::operator<(const Vec2Int &other) const {
    return this->x == other.x ? this->y < other.y : this->x < other.x;
}

Vec2Int Vec2Int::operator+(const Vec2Int &other) const {
    return {this->x + other.x, this->y + other.y};
}

Vec2Int Vec2Int::operator-(const Vec2Int &other) const {
    return {this->x - other.x, this->y - other.y};
}

Vec2Int Vec2Int::operator-() const {
    return {-this->x, -this->y};
}

size_t std::hash<Vec2Int>::operator()(const Vec2Int &value) const {
    size_t result = 0;
    result ^= std::hash<int>{}(value.x) + 0x9e3779b9 + (result << 6) + (result >> 2);
    result ^= std::hash<int>{}(value.y) + 0x9e3779b9 + (result << 6) + (result >> 2);
    return result;
}
