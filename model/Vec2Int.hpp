#ifndef _MODEL_VEC2_INT_HPP_
#define _MODEL_VEC2_INT_HPP_

#include "../Stream.hpp"
#include <string>
#include <array>

class Vec2Int {
public:
    int x;
    int y;
    Vec2Int();
    Vec2Int(int x, int y);
    static Vec2Int readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const;
    bool operator ==(const Vec2Int& other) const;
    bool operator !=(const Vec2Int &other) const;

    bool operator <(const Vec2Int& other) const;
    Vec2Int operator +(const Vec2Int& other) const;
    Vec2Int operator -(const Vec2Int& other) const;
    Vec2Int operator -() const;

    bool match(int x, int y) const;
    int distance(const Vec2Int &other) const;

    inline bool oneIsZero() const {
        return x == 0 || y == 0;
    }

    std::array<Vec2Int, 4> neighbours() const;

    std::array<Vec2Int, 4> neighboursShuffled() const;
};
namespace std {
    template<>
    struct hash<Vec2Int> {
        size_t operator ()(const Vec2Int& value) const;
    };
}

#endif
