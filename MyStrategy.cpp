#include "MyStrategy.hpp"
#include "myModel/Utils.h"
#include "control/Movements.h"
#include "control/ResourceCollection.h"
#include "control/Building.h"
#include "control/Fields.h"
#include <exception>
#include <algorithm>
#include <iostream>
#include <memory>
#include <queue>
#include "control/TimeMeasure.h"
#include "control/UnitProducing.h"
#include "control/Fighting.h"

MyStrategy::MyStrategy() : newDamage(0), newKills(0) {

}

void initOnStart(const PlayerView &playerView) {
    if (playerView.currentTick != 0) {
        return;
    }
    Data::initMap(playerView.mapSize);
    PathMap::init();
    Data::init();
    Mark::field[LAST_SAW].fill(-1);
    for (auto &player : playerView.players) {
        if (player.id == playerView.myId) {
            Data::me.player = player;
        } else {
            Data::others.emplace_back();
            Data::others.rbegin()->player = player;
        }
    }

    Data::myId = playerView.myId;
    auto maxId = std::max_element(playerView.entityProperties.begin(),
                                  playerView.entityProperties.end(),
                                  [](const auto &first, const auto &second) {
                                      return first.first < second.first;
                                  })->first;
    MyEntity::props = std::vector<EntityProperties>();
    auto &props = MyEntity::props;

    Data::healCounter = 0;
    props.resize(maxId + 1);
#ifdef DEBUG_ENABLED
    std::cout << props.size() << std::endl;
#endif
    for (const auto &val : playerView.entityProperties) {
        props[val.first] = val.second;
    }
    if (playerView.fogOfWar) {
        auto builder = MyEntity(Entity(-1,
                                       std::shared_ptr<int>(0),
                                       BUILDER_UNIT,
                                       Vec2Int(0, 0),
                                       props[BUILDER_UNIT].maxHealth,
                                       true));
        auto base = MyEntity(Entity(-1,
                                    std::shared_ptr<int>(0),
                                    BUILDER_BASE,
                                    Vec2Int(0, 0),
                                    props[BUILDER_BASE].maxHealth,
                                    true));
        //Entity(int id, std::shared_ptr<int> playerId, EntityType entityType, Vec2Int position, int health, bool active);
        for (auto &other: Data::others) {
            builder.playerId = other.id();
            base.playerId = other.id();
            switch (playerView.myId) {
                case 1:
                    switch (other.id()) {
                        case 2:
                            other.workers.emplace_back(builder, Vec2Int(75, 75));
                            other.builderBase.emplace_back(base, Vec2Int(70, 70));
                            break;
                        case 3:
                            other.workers.emplace_back(builder, Vec2Int(75, 4));
                            other.builderBase.emplace_back(base, Vec2Int(70, 5));
                            break;
                        case 4:
                            other.workers.emplace_back(builder, Vec2Int(4, 75));
                            other.builderBase.emplace_back(base, Vec2Int(5, 70));
                            break;
                    }
                    break;
                case 2:
                    switch (other.id()) {
                        case 1:
                            other.workers.emplace_back(builder, Vec2Int(75, 75));
                            other.builderBase.emplace_back(base, Vec2Int(70, 70));
                            break;
                        case 3:
                            other.workers.emplace_back(builder, Vec2Int(4, 75));
                            other.builderBase.emplace_back(base, Vec2Int(5, 70));
                            break;
                        case 4:
                            other.workers.emplace_back(builder, Vec2Int(75, 4));
                            other.builderBase.emplace_back(base, Vec2Int(70, 5));
                            break;
                    }
                    break;
                case 3:
                    switch (other.id()) {
                        case 1:
                            other.workers.emplace_back(builder, Vec2Int(4, 75));
                            other.builderBase.emplace_back(base, Vec2Int(5, 70));
                            break;
                        case 2:
                            other.workers.emplace_back(builder, Vec2Int(75, 4));
                            other.builderBase.emplace_back(base, Vec2Int(70, 5));
                            break;
                        case 4:
                            other.workers.emplace_back(builder, Vec2Int(75, 75));
                            other.builderBase.emplace_back(base, Vec2Int(70, 70));
                            break;
                    }
                    break;
                case 4:
                    switch (other.id()) {
                        case 1:
                            other.workers.emplace_back(builder, Vec2Int(75, 4));
                            other.builderBase.emplace_back(base, Vec2Int(70, 5));
                            break;
                        case 2:
                            other.workers.emplace_back(builder, Vec2Int(4, 75));
                            other.builderBase.emplace_back(base, Vec2Int(5, 70));
                            break;
                        case 3:
                            other.workers.emplace_back(builder, Vec2Int(75, 75));
                            other.builderBase.emplace_back(base, Vec2Int(70, 70));
                            break;
                    }
                    break;
            }
        }
    }
}

Action MyStrategy::getAction(const PlayerView &playerView, DebugInterface *debugInterface) {
#ifdef DEBUG_ENABLED
    std::cout << playerView.currentTick << std::endl;
#endif
    if (!playerView.entities.empty()) {
        Data::maxEntityId = playerView.entities.rbegin()->id;
    }
    TimeMeasure::start();
    initOnStart(playerView);
    PathMap::nextTick();
    for (int i = 0; i < MY_DISTANCE_FIELD; ++i) {
        Mark::field[i].fill(std::numeric_limits<int>::max());
    }
    for (int i = MY_DISTANCE_FIELD; i <= ENEMY_CIVILIAN_DISTANCE_FIELD; ++i) {
        Mark::field[i].fill(Mark::NO_ENEMY_DISTANCE);
    }
    for (int i = ENEMY_DANGER_FIELD; i <= ENEMY_DANGER_EXTENDED_FIELD_2; ++i) {
        Mark::field[i].fill(0);
    }
//    for (int i = ME_DANGER_FIELD; i <= ME_DANGER_EXTENDED_FIELD_2; ++i) {
//        Mark::field[i].fill(0);
//    }
    Mark::field[LAST_PATH_HOTSPOT].copy(Mark::field[PATH_HOTSPOT]);

    PlayerData &me = Data::me;
    me.update(playerView);
    if (!me.rangedBase.empty()) {
        me.startedBarrack = true;
    }
    auto &others = Data::others;
    bool neiboursStartedBarrack = false;
    if (others.size() == 1) {
        neiboursStartedBarrack = others[0].startedBarrack;
    } else {
        int nIdStart = (((me.id() + 1) & 6) + 1) % 4;
        for (const auto &other : others) {
            if (other.id() < nIdStart || other.id() > nIdStart + 1) {
                continue;
            }
            neiboursStartedBarrack |= other.startedBarrack;
        }
    }

    auto &fogOfWar = Mark::field[VISIBLE];
    Data::initMap(playerView.mapSize);
    if (playerView.fogOfWar) {
        auto &lastSawField = Mark::field[LAST_SAW];
        lastSawField.currentMark = playerView.currentTick;
        Fields::calcFogOfWar(me, fogOfWar);
#ifdef REWIND_VIEWER
        MapDrawing::drawYesNoField(fogOfWar, 0x33000000, 9, true);
#endif
    }
    for (auto &other : others) {
        other.update(playerView);
    }

//    int resourcesCount = std::count_if(playerView.entities.begin(), playerView.entities.end(), [](const auto &entity) {
//        return entity.entityType == EntityType::RESOURCE;
//    });
    auto &resources = Data::resources;
    {
        auto &lastSawField = Mark::field[LAST_SAW];
        auto &knownResource = Mark::instances[0];
        ++knownResource.currentMark;
        for (const auto &old : resources) {
            knownResource.putMarkUnsafe(old.position);
        }
        std::vector<MyEntity> newResources;
        newResources.reserve(resources.size() + 40);
        std::vector<int> newVeins;
        for (const auto &entity: playerView.entities) {
            if (entity.entityType != EntityType::RESOURCE) {
                continue;
            }
            newResources.emplace_back(entity, playerView.currentTick);

            // оп, новая жилка! надо везде добавить. А что если мы увидели две жилки сразу....? Раздвоение = непорядок
            if (!knownResource.checkMarkUnsafe(entity.position)) {
                newVeins.emplace_back(newResources.size() - 1);
                knownResource.putMarkUnsafe(entity.position);
            }
        }

        for (const auto &id : newVeins) {
            for (const auto &pt : Utils::allMirroredPoints(newResources[id].position, playerView.players.size())) {
                if (knownResource.checkMarkUnsafe(pt) || lastSawField.getMarkUnsafe(pt) >= 0) {
                    continue;
                }
                newResources.emplace_back(newResources[id], pt);
                knownResource.putMarkUnsafe(pt);
            }
        }
        ++knownResource.currentMark;
        for (const auto &updated : newResources) {
            knownResource.putMarkUnsafe(updated.position);
        }
        // восстановить всё невидимое на место
        for (const auto &myEntity : resources) {
            if (!knownResource.checkMarkUnsafe(myEntity.position) && !fogOfWar.checkMarkUnsafe(myEntity.position)) {
                newResources.emplace_back(myEntity);
            }
        }
        resources.swap(newResources);
        Data::addAllToMap(resources);
    }

    me.applyForAll(Data::addAllToMap);
    for (auto &other : others) {
        other.applyForAll(Data::addAllToMap);
    }
    Fields::updateTimeToGoFields();

    Fields::applyDangerFields(others);
//    {
//        Fields::applyDangerFields(me,
//                                  Mark::field[ME_DANGER_FIELD],
//                                  Mark::field[ME_DANGER_EXTENDED_FIELD],
//                                  Mark::field[ME_DANGER_EXTENDED_FIELD_2]);
////        MapDrawing::drawOverlapField(Mark::field[ME_DANGER_FIELD], 0x00FF00, 7);
////        MapDrawing::drawOverlapField(Mark::field[ME_DANGER_EXTENDED_FIELD], 0x00FF00, 8);
////        MapDrawing::drawOverlapField(Mark::field[ME_DANGER_EXTENDED_FIELD_2], 0x00FF00, 9);
//    }


    Fields::calcMilDistanceField(me, Mark::field[MY_DISTANCE_FIELD]);
    Fields::calcWorkersDistanceField(me, Mark::field[MY_WORKERS_DISTANCE_FIELD]);
    for (auto &other : others) {
        Fields::calcWorkersDistanceField(other, Mark::field[ENEMY_WORKERS_DISTANCE_FIELD]);
    }
//    MapDrawing::drawGradientField(Mark::field[MY_WORKERS_DISTANCE_FIELD], std::numeric_limits<int>::max(), 8);


    for (auto &other : others) {
        Fields::calcMilDistanceField(other, Mark::field[ENEMY_MIL_DISTANCE_FIELD]);
        Fields::calcCivDistanceField(other, Mark::field[ENEMY_CIVILIAN_DISTANCE_FIELD]);
    }
    {
        auto &myDistance = Mark::field[MY_DISTANCE_FIELD];
        auto &enemyDistance = Mark::field[ENEMY_MIL_DISTANCE_FIELD];
        auto &influenceField = Mark::field[INFLUENCE_FIELD];
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                influenceField.mark[i][j] = enemyDistance.mark[i][j] - myDistance.mark[i][j];
            }
        }
//        MapDrawing::drawGradientField(myDistance, Mark::NO_ENEMY_DISTANCE, 7);
//        MapDrawing::drawGradientField(enemyDistance, Mark::NO_ENEMY_DISTANCE, 8);
//        MapDrawing::drawGradientWithMidField(influenceField, 2);
    }
    TimeMeasure::end(0);

#ifdef REWIND_VIEWER
    MapDrawing::drawMap(playerView, me, others);
    TimeMeasure::start();
#endif
    auto &staticBlockField = Mark::field[BLOCK_STATIC];
    ++staticBlockField.currentMark;
    for (int i = 0; i != Data::mapSize; ++i) {
        for (int j = 0; j != Data::mapSize; ++j) {
            auto entity = Data::getUnsafe(i, j);
            if (entity == nullptr) {
                continue;
            }
            switch (entity->entityType) {
                case BUILDER_UNIT:
                case RANGED_UNIT:
                case MELEE_UNIT:
                    if (entity->playerId != Data::myId) {
                        staticBlockField.putMark({i, j});
                    }
                    break;
                default:
                    staticBlockField.putMark({i, j});
            }
        }
    }
//    MapDrawing::drawYesNoField(staticBlockField, 0, 7);
    TimeMeasure::end(1);
    ResourceCollection::find_best_dig_pairs(me, resources);
    me.player.resource += Data::expectedResource;
    Mark::field[PATH_HOTSPOT].fill(0);
    TimeMeasure::end(2);
    Building::repair(me);
    TimeMeasure::end(3);

    auto unitsToBuild = UnitProducing::get_units_needed(me, others, resources, playerView);
#ifdef REWIND_VIEWER
    RewindClient::instance().message("Units to build W %d M %d R %d",
                                     unitsToBuild.workers,
                                     unitsToBuild.melee,
                                     unitsToBuild.ranged);
#endif
    if (me.rangedBase.empty() && me.player.resource >= MyEntity::props[RANGED_BASE].cost) {
        Building::build_smth(me, 1, RANGED_BASE);

        unitsToBuild = UnitProducing::get_units_needed(me, others, resources, playerView);
#ifdef REWIND_VIEWER
        RewindClient::instance().message("UPD after ranged base! Units to build W %d M %d R %d",
                                         unitsToBuild.workers,
                                         unitsToBuild.melee,
                                         unitsToBuild.ranged);
#endif
    }
    if (me.builderBase.empty() && me.player.resource >= MyEntity::props[BUILDER_BASE].cost && unitsToBuild.workers) {
        Building::build_smth(me, 1, BUILDER_BASE);
        unitsToBuild = UnitProducing::get_units_needed(me, others, resources, playerView);
#ifdef REWIND_VIEWER
        RewindClient::instance().message("UPD after builders base! Units to build W %d M %d R %d",
                                         unitsToBuild.workers,
                                         unitsToBuild.melee,
                                         unitsToBuild.ranged);
#endif
    }
    int housesNeeded = Building::houses_needed(me);
    if (!playerView.fogOfWar || !neiboursStartedBarrack || me.startedBarrack) {
        UnitProducing::buildUnits(me, unitsToBuild);
        if (playerView.fogOfWar) {
            if (me.totalHouses < UnitProducing::maxWorkers || me.startedBarrack) {
                Building::build_smth(me, housesNeeded, HOUSE);
            }
        } else {
            Building::build_smth(me, housesNeeded, HOUSE);
        }
    } else {
        if (me.player.resource >=
        MyEntity::props[BUILDER_UNIT].cost + (int) me.workers.size() + MyEntity::props[RANGED_BASE].cost) {
            UnitProducing::buildUnits(me, unitsToBuild);
        }
        if (me.player.resource >= MyEntity::props[HOUSE].cost + MyEntity::props[RANGED_BASE].cost &&
                me.totalHouses + 20 < UnitProducing::maxWorkers) {
// снижаем лимит, оч нужны казармы
            Building::build_smth(me, housesNeeded, HOUSE);
        }
    }
    Data::addAllToMap(me.newBuildings);
    TimeMeasure::end(4);
    if (!playerView.fogOfWar || me.startedBarrack) { // только после постройки бараков
        int maxTurretsCntTb = (me.player.resource - 1000) / MyEntity::props[TURRET].cost;
        if (maxTurretsCntTb > 0) {
            int tbcnt = 0;
            for (const auto &turr : me.turrets) {
                if (!turr.active) {
                    ++tbcnt;
                }
            }
            Building::build_smth(me, std::min(3 - tbcnt, maxTurretsCntTb), TURRET);
        }
    }
    TimeMeasure::end(5);

    int range = 1;
    auto applyStaticForAttackers = [&range = range, &staticBlockField](const MyEntity &entity) {
        if (Utils::hasEnemiesInAttackRange(range, entity)) {
            staticBlockField.putMarkUnsafe(entity.position);
        }
    };
    std::for_each(me.meleeUnits.begin(), me.meleeUnits.end(), applyStaticForAttackers);
    range = 5;
    std::for_each(me.rangedUnits.begin(), me.rangedUnits.end(), applyStaticForAttackers);
//    // apply static for repairers
//    std::for_each(me.workers.begin(), me.workers.end(), [&staticBlockField](const MyEntity &entity) {
//        if (entity.action && entity.action->actionType == REPAIR && entity.action->target == entity.position) {
//            staticBlockField.putMarkUnsafe(entity.position);
//        }
//    });

    TimeMeasure::end(6);
    Action action;
    Movements::applyWorkerActions(me.workers, action.entityActions);
//    MapDrawing::drawYesNoField(staticBlockField, 0, 5);
    for (auto &worker : me.workers) {// leave builders do they job
        if (worker.action && worker.action->actionType != MOVE_TO) {
            staticBlockField.putMarkUnsafe(worker.position);
        }
    }
//    MapDrawing::drawYesNoField(staticBlockField, 0, 6);
    Movements::applyBasesActions(me.meleeBase, action.entityActions);
    Movements::applyBasesActions(me.rangedBase, action.entityActions);
    Movements::applyBasesActions(me.builderBase, action.entityActions);
    TimeMeasure::end(7);
    Fighting::applyArchersAttack(me.rangedUnits, action.entityActions);
    TimeMeasure::end(9);
    Fighting::applyAttackActions(me.meleeUnits, action.entityActions);
    Fighting::applyTurretActions(me.turrets, action.entityActions);

    std::vector<MyEntity *> attackingEntities;
    auto attackFilter = [&attackingEntities](MyEntity &entity) {
        if (entity.action && entity.action->actionType == ATTACK_TARGET) {
            attackingEntities.push_back(&entity);
        }
    };
    std::for_each(me.rangedUnits.begin(), me.rangedUnits.end(), attackFilter);
    std::for_each(me.meleeUnits.begin(), me.meleeUnits.end(), attackFilter);
    std::for_each(me.turrets.begin(), me.turrets.end(), attackFilter);
    while (true) {
        auto newData = Fighting::rearrangeAttacks(attackingEntities, action.entityActions);
        newDamage += newData.first;
        newKills += newData.second;
        if (newData.first == 0 && newData.second == 0) {
            break;
        }
    }

//    me.workers
//    me.applyForAll([&entityAction = action.entityActions](const std::vector<MyEntity> &dest) {
//        Movements::applyActions(dest, entityAction);
//    });
    TimeMeasure::end(10);
#ifdef REWIND_VIEWER
    RewindClient::instance().message("NewDamage %d, NewKills %d", newDamage, newKills);
    RewindClient::instance().message("Houses needed %d", housesNeeded);
    RewindClient::instance().message("Me:   W %d R %d M %d",
                                     me.workers.size(),
                                     me.rangedUnits.size(),
                                     me.meleeUnits.size());
    for (const auto &other : others) {
        RewindClient::instance().message("id: %d W %d R %d M %d",
                                         other.id(),
                                         other.workers.size(),
                                         other.rangedUnits.size(),
                                         other.meleeUnits.size());
    }

    MapDrawing::drawOverlapField(Mark::field[PATH_HOTSPOT], 0xFFFF00, 7);
    me.applyForAll(MapDrawing::drawEntityAction);

    for (const auto &newBuilding : me.newBuildings) {
        MapDrawing::drawEntity(newBuilding, playerView, MapDrawing::players_colors);
    }
#ifdef DEBUG_ENABLED
    RewindClient::instance().message("A Atk %d K %d DR %d D %d",
                                     Data::attack[RANGED_UNIT],
                                     Data::kills[RANGED_UNIT],
                                     Data::damageReceived[RANGED_UNIT],
                                     Data::deaths[RANGED_UNIT]);
    RewindClient::instance().message("W DR %d D %d Atk %d K %d",
                                     Data::damageReceived[BUILDER_UNIT],
                                     Data::deaths[BUILDER_UNIT],
                                     Data::attack[BUILDER_UNIT],
                                     Data::kills[BUILDER_UNIT]);
    RewindClient::instance().message("OverDamage: %d", Data::overDamage);

    for (auto &other : others) {
        auto calc = [](const std::vector<MyEntity> &entities) {
            for (const auto &entity: entities) {
                if (entity.actionsCount == 0) {
                    continue;
                }
                if (entity.actionsCount >= entity.health) {
                    Data::attack[entity.entityType] += entity.health;
                    Data::overDamage += entity.actionsCount - entity.health;
                    ++Data::kills[entity.entityType];
                } else {
                    Data::attack[entity.entityType] += entity.actionsCount;
                }
            }
        };
        other.applyForAll(calc);
    }
#endif
    RewindClient::instance().end_frame();
#endif

    if (playerView.currentTick % 100 == 99) {
        TimeMeasure::printTimings();
    }
    return action;
}

void MyStrategy::debugUpdate(const PlayerView &playerView, DebugInterface &debugInterface) {
    debugInterface.send(DebugCommand::Clear());
    debugInterface.getState();
}